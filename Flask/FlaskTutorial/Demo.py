from flask import Flask, render_template,session, request, redirect, url_for, flash, jsonify,json,Response,make_response
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_mail import Mail, Message
from itsdangerous import URLSafeTimedSerializer
from werkzeug.security import generate_password_hash,check_password_hash
import jwt
import datetime
from functools import wraps

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/raghudungala/Raghu/Flask/FlaskTutorial/test.db'
app.config['SECRET_KEY'] = 'the random string'
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'draghu222@gmail.com'
app.config['MAIL_PASSWORD'] = 'Dura@10387'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

s = URLSafeTimedSerializer('thisisasecreat')

mail = Mail(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)


class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    completed = db.Column(db.Integer, default=0)

    def __repr__(self):
        return '<Task %r>' % self.id


class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('todo.id'))
    user = db.relationship('Todo', backref='Student')


class Register(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), nullable=False)
    password = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.String(200), nullable=False)
    gmail = db.Column(db.String(200), nullable=False)


class RegisterSchema(ma.Schema):
    class Meta:
        model = Register
        fields = ("id", 'username', 'password', 'phone')

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'auth_token' in session:
            token = session['auth_token']

            if not token:
                flash("Invalid Token","danger")
                return redirect(url_for('login'))
            else:
                try: 
                    data = jwt.decode(token, app.config['SECRET_KEY'])
                    current_user = Register.query.filter_by(username=data['username']).first()
                except:
                    flash("Invalid Token","danger")
                    return redirect(url_for('login'))

                return f(current_user, *args, **kwargs)
        else:
            flash("Invalid Token","danger")
            return redirect(url_for('login'))

    return decorated


@app.route('/',methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'] )
def login():
    if request.method == 'POST':
    # auth = request.authorization
        username = request.form['username']
        password = request.form['password']
        if not username or not password:
            return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

        user = Register.query.filter_by(username=username).first()

        if not user:
            return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

        if check_password_hash(user.password, password):
            token = jwt.encode({'username' : username, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])
            session['auth_token']=token
        return redirect(url_for('home'))

            # return jsonify({'token' : token.decode('UTF-8')})

        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})
    return render_template('login.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        hashed_password=generate_password_hash(request.form['password'],method='sha256')
        new_rec = Register(
            username=request.form["username"] ,
            password=hashed_password,
            phone=request.form['phone'],
            gmail=request.form['email']
        )
        gmail = request.form['email']
        token = s.dumps(gmail, salt='email-confirm')
        msg = Message('confirm-Email',
                      sender='draghu222@gmail.com', recipients=[gmail])
        link = url_for('confirm_email', token=token, _external=True)
        msg.body = 'Please Click to conifrm the your account is {}'.format(
            link)
        mail.send(msg)
        db.session.add(new_rec)
        db.session.commit()
        new_Schema = RegisterSchema().dump(new_rec).data
        # return "Sent"
        # return redirect(url_for('login'))
        return jsonify({"success": 200, "message": "successfully created", "users": new_Schema})
    return render_template('register.html')


@app.route('/confirm-email/<token>')
def confirm_email(token):
    try:
        email = s.loads(token, salt='email-confirm', max_age=180)
    except SignatureExpired:
        return '<h2>The token Expired<h2>'
    return render_template('confirm.html')


@app.route('/home', methods=['GET'])
@token_required
def home(current_user):
    users = Register.query.all()
    Total_users = []
    if Response.json:
        for user in users:
            users_data={}
            users_data['username']=user.username 
            users_data['password']=user.password 
            users_data['phone']=user.phone 
            users_data['gmail']=user.gmail
            Total_users.append(users_data)
        # return jsonify({"success": 200, "message": "successfully created"},{'users':Total_users})
    return render_template('home.html',users=users,username=current_user.username)


@app.route('/search/<username>', methods=["GET"])
@token_required
def show_user(current_user,username):
    user = Register.query.filter_by(username=username).first_or_404()
    if request.json:

        users_data={}
        users_data['username']=user.username
        users_data['phone']=user.phone
        users_data['gmail']=user.gmail
        return jsonify({"success": 200, "message": "successfully created"},{"user":users_data})
    elif request.form:
        return render_template('search.html')
    return render_template('search.html',user=user,username=current_user.username)



@app.route('/edit/<username>', methods=["GET", "POST"])
@token_required
def Edit_user(current_user,username):
    user=Register.query.filter_by(username=username).first()
    if request.method == 'POST':
        user.username= request.form['username'] 
        user.phone= request.form['phone'] 
        user.gmail= request.form['email']
        db.session.commit()
        # return jsonify({"success": 200, "message": "successfully created"})
        return redirect(url_for('home'))
    return render_template('Edit.html', user=user,username=current_user.username)


@app.route('/delete/<username>',methods=["GET","DELETE"])
@token_required
def Delete(current_user,username):
    Register.query.filter_by(username=username).delete()
    db.session.commit()
    return jsonify({"success": 200, "message": "successfully Deleted"})

    return redirect(url_for('home'))


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))



if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)
