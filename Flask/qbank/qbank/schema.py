from qbank import db, ma

# Roles Table
# class roles(db.Model):
#      id = db.Column('id', db.Integer, primary_key = True)
#      name = db.Column(db.String(100),nullable=False)
#      def __init__(self, id,name):
#          self.id = id
#          self.name = name
#
#question_type Table
class Question_type(db.Model):
     __tablename__ = 'question_types'
     id = db.Column('id', db.Integer, primary_key = True)
     name = db.Column(db.String(100),nullable = False)
     questions = db.relationship("Question", backref="question_types")
     def __init__(self, name):
         self.name = name


class Question(db.Model):
   __tablename__ = 'questions'
   id = db.Column('id', db.Integer, primary_key = True)
   summary = db.Column(db.Text, unique=True, nullable=False)
   description = db.Column(db.Text,unique=True,nullable=False)
   weightage = db.Column(db.Integer,nullable=False)
   user_id = db.Column(db.Integer,nullable=False)

   qtype_id = db.Column(db.Integer, db.ForeignKey('question_types.id'))
   skill_id = db.Column(db.Integer, db.ForeignKey('skills.id'))
   category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))

   def __init__(self,summary,description,weightage,user_id,qtype_id,skill_id,category_id):
       #self.id = id
       self.description = description
       self.summary = summary
       self.weightage = weightage
       self.user_id = user_id
       self.qtype_id = qtype_id
       self.skill_id = skill_id
       self.category_id = category_id


class QuestionSchemaUpdateCreate(ma.Schema):
  class Meta:
    model = Question
    fields = ("id", 'summary','description','weightage','user_id', 'qtype_id','skill_id','category_id')


class Category(db.Model):
    __tablename__ = 'categories'
    id = db.Column('id', db.Integer, primary_key = True)
    name = db.Column(db.String(100),unique=True,nullable = False)
    skills = db.relationship("Skill", backref="category")
    questions = db.relationship("Question", backref="category")

    def __init__(self,name):
        self.name = name


class Skill(db.Model):
    __tablename__ = 'skills'
    id = db.Column('id', db.Integer, primary_key = True)
    name = db.Column(db.String(100),unique=True,nullable = False)
    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
    questions = db.relationship('Question', backref="skill")

    def __init__(self, name,category_id):
        #self.id = id
        self.name = name
        self.category_id = category_id


class Level(db.Model):
    __tablename__ = 'levels'
    id = db.Column('id', db.Integer, primary_key = True)
    name = db.Column(db.String(100),nullable = False)
    weightage_factor = db.Column(db.Float,nullable = False)

    def __init__(self, name, weightage_factor):
        self.name = name
        self.weightage_factor = weightage_factor


class User(db.Model):
    __tablename__='user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(10))
    #email = db.Column(db.String(120), unique=True)
    def __init__(self, username, password):
        self.username = username
        self.password = password