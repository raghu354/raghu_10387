from qbank import db
from flask import request, render_template, jsonify, Blueprint, json
# application modules
from qbank.schema import Category, Skill, Question, Question_type, QuestionSchemaUpdateCreate
# from werkzeug.datastructures import ImmutableMultiDict

author = Blueprint('author', __name__)

def question_obj(id):
  return Question.query.filter_by(id=id).first()

@author.route('/author')
def index():
    category = Category.query.all()
    skills = Skill.query.all()
    qtypes = Question_type.query.all()

    return render_template('author.html',category=category,skills=skills,qtypes=qtypes)

@author.route('/addquestion', methods=['POST', "GET"])
def add_questions():

    sid = request.form["sid"]
    new_rec = Question(
                request.form["summary"],
                request.form['description'],
                request.form['weightage'],
                request.form['user_id'],
                request.form['qtype_id'],
                sid,
                request.form['cid']
              )
    try:
      db.session.add(new_rec)
      db.session.commit()
      question_shema = QuestionSchemaUpdateCreate().dump(new_rec).data
      return jsonify({"success": 200, "message": "successfully created", "question": question_shema})
    except Exception as err:
      print("Failed to create.")
      print(err)
      return jsonify({"status": 404, "message": "error to create"})

@author.route("/delete/<int:id>", methods=["DELETE"])
def delete(id):
  try:
    question = question_obj(id)
    db.session.delete(question)
    db.session.commit()
    return jsonify({"success": 200, "message": "successfully deleted", "id": id})
  except Exception as err:
    print("Failed to delete.")
    print(err)
    return jsonify({"status": 404, "message": "error to delete"})

@author.route("/update/<int:id>", methods=["PUT"])
def update(id):
  try:
      question = question_obj(id)
      # form = json.loads(request.form.to_dict().keys()[0]);
      # question.tag = form["tag"]
      # question.description = form["description"]
      # question.summary = form["summary"]
      # question.weightage = form["weightage"]
      # question.qtype_id = form["qtype_id"]
      # question.category_id = form["category_id"]
      # question.skill_id = form["skill_id"]
      question.description = request.args.get("description")
      question.summary = request.args.get("summary")
      question.weightage = request.args.get("weightage")
      question.qtype_id = request.args.get("qtype_id")
      question.category_id = request.args.get("category_id")
      question.skill_id = request.args.get("skill_id")
      question_shema = QuestionSchemaUpdateCreate().dump(question).data
      db.session.merge(question)
      db.session.commit()
      db.session.close()
      return jsonify({"success": 200, "message": "successfully updated", "id": id, "question": question_shema})
  except Exception as err:
    print("Failed to update.")
    print(err)
    return jsonify({"status": 404, "message": "error to update"})

if __name__ == '__main__':
    #db.create_all()
    app.run(debug = True)
