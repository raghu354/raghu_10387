from qbank import db
from flask import request, render_template, jsonify, Blueprint
# application modules
from qbank.schema import Category, Skill, Level, Question, Question_type

import pdb;

application = Blueprint('application', __name__)

@application.route('/question', methods=['GET','POST'])
def questions():
    questions = Question.query.filter_by(
                skill_id= request.args.get("skill_list"),
                qtype_id= request.args.get("qtype"),
                category_id= request.args.get("category_list"),
                weightage= request.args.get("weightage")
              )
    return render_template('questions.html', questions=questions)

@application.route('/question/<int:id>', methods=["GET"])
def question(id):
    try:
      category = Category.query.all()
      skills = Skill.query.all()
      qtypes = Question_type.query.all()
      levels = Level.query.all()

      question = db.session.query(
                    Question.user_id,
                    Question.id,
                    Question.description,
                    Question.weightage,
                    Question.summary,
                    (Skill.id).label("sid"),
                    (Category.id).label("cid"),
                    (Question_type.id).label("q_type_id"),
                    (Skill.name).label("skill"),
                    Category.name.label("category"),
                    Question_type.name.label("qtype")
                  ).filter_by(id=id).\
                  join(Skill, Skill.id==Question.skill_id).\
                  join(Question_type, Question_type.id==Question.qtype_id).\
                  join(Category, Category.id==Skill.category_id).\
                  first()
      weightages = {}
      for level in levels:
        weightages[level.name] = round(float(question.weightage)*float(level.weightage_factor), 2)

      form = ('edit.html' if request.args.get("form_type")=="#editQuestion" else "show.html")
      return render_template(form, question=question,category=category,skills=skills,qtypes=qtypes,levels=levels,weightages=weightages)
    except Exception as err:  
      print("Failed to find the question.")
      print(err)