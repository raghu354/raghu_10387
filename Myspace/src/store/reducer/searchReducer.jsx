import * as allActions from '../actions/actionConstants'
const initialState = {
    details: {},
    isLoaded: false,
    statuscode:""
}
export default function searchreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.SEARCH_EMPLOYEE_BY_NAME:
            return action;

        case allActions.SEARCH_EMPLOYEE_BY_EMAIL:
            return action;

        case allActions.GET_EMPLOYEE_DETAILS:
            debugger;
            return {
                ...state,
                isLoaded: true,
                details: action.payload
            }
        case allActions.SEARCH_EMPLOYEE_BY_NAME_ERROR:
        console.log(action.payload)
        return{
            ...state,
            statuscode:action.payload
        }
        default:
            return state;
    }
}