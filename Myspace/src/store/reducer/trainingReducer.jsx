import * as allActions from '../actions/actionConstants';


const initialise = {
    events: [],
    isLoaded: false
}

export default function trainingReducer(state = initialise, action) {
    switch (action.type) {
        case allActions.FETCH_TIME:
        
            return action;

        case allActions.RECEIVE_TIME:
            return {
                ...state,
                events: action.payload,
                isLoaded: true
            }

        default:
            return state
    }
}