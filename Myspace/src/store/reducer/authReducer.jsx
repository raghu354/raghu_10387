import * as allActions from '../actions/actionConstants'

const initialState = {
    is_logged_in : false,
    user_details : {},
    my_profile : false,
    update_profile : false,
    statuscode:"",
    edit_statuscode:""
}

export default function authReducer(state = initialState,action){
    switch(action.type){
        case allActions.DO_LOGIN_USER:
        debugger;
          return action
        case allActions.LOGIN_USER_SUCCESS:
            console.log("inside auth reducer");
            console.log(action.payload);
                return{
                    ...state,
                    is_logged_in : true,
                    
                }
                
        case allActions.MY_PROFILE_RECEIEVE:
        debugger;
        console.log("inside profile reducer");
        console.log(action.payload);
                return{
                    ...state,
                    user_details : action.payload,
                    my_profile : true,
                    statuscode:action.payload.statuscode,
                }
        case allActions.LOGIN_USER_DATA_ERROR:
            debugger
            console.log(action.payload)
            return{
                ...state,
                statuscode:action.payload
            }
        case allActions.EDIT_PROFILE_SUCCESS:
        return{
            ...state,
            update_profile : true,
            edit_statuscode:action.payload.statuscode
        }

        default:
            return state;
    }
}