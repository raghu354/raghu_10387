import * as allActions from './actionConstants'

export function searchByName(nameid) {
    debugger;
    console.log(nameid);
    return {
        type: allActions.SEARCH_EMPLOYEE_BY_NAME,
        payload: {nameid}
    }
}
export function searchByEmail(nameid) {
    debugger;
    console.log(nameid);
    return {
        type: allActions.SEARCH_EMPLOYEE_BY_EMAIL,
        payload: {nameid}
    }
}


export function getEmpDetail(data) {
    return {
        type: allActions.GET_EMPLOYEE_DETAILS,
        payload : data
    }
}