
 export const DO_LOGIN_USER         = 'DO_LOGIN_USER'
 export const LOGIN_USER_SUCCESS    = 'LOGIN_USER_SUCESS'
 export const LOGIN_USER_DATA_ERROR = 'LOGIN_USER_DATA_ERROR'

 export const MY_PROFILE            = 'MY_PROFILE'
 export const MY_PROFILE_RECEIEVE   = 'MY_PROFILE_RECEIEVE'

 export const EDIT_PROFILE          = 'EDIT_PROFILE'
 export const EDIT_PROFILE_SUCCESS  = 'EDIT_PROFILE_SUCCESS'

 export const FETCH_EVENTS          = 'FETCH_EVENTS'
 export const RECEIVE_EVENTS        = 'RECEIVE_EVENTS'

 export const DELETE_EVENTS         = 'DELETE_EVENTS'

 export const LOGOUT_USER           = 'LOGOUT_UER'
 
 export const FETCH_TIME            = 'FETCH_TIME'
 export const RECEIVE_TIME          = 'RECEIEVE_TIME'

 export const GET_SALARY            = 'GET_SALARY'
 export const RECEIEVE_SALARY       = 'RECEIEVE_SALARY'

 export const POST_LEAVES           = 'POST_LEAVES'
 export const POST_LEAVES_SUCCESS   = 'POST_LEAVES_SUCCESS' 
 export const POST_LEAVES_DATA_ERROR = 'POST_LEAVES_DATA_ERROR'


 export const SEARCH_EMPLOYEE_BY_NAME = 'SEARCH_EMPLOYEE_BY_NAME'
 export const GET_EMPLOYEE_DETAILS  = 'GET_EMPLOYEE_DETAILS'
 export const SEARCH_EMPLOYEE_BY_EMAIL  = 'SEARCH_EMPLOYEE_BY_EMAIL'
 export const SEARCH_EMPLOYEE_BY_NAME_ERROR='SEARCH_EMPLOYEE_BY_NAME_ERROR'