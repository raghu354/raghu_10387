import * as allActions from './actionConstants'

export  function receiveEvents(data) {
    return {
        type: allActions.RECEIVE_EVENTS, payload: data
    }
}

export  function fetchEvents() {
    return {
        type: allActions.FETCH_EVENTS, payload: {}
    }
}

export function deleteEvent(id) {
    debugger;
    return {
        type : allActions.DELETE_EVENTS, payload : {id}
    }
}
