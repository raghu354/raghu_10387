import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import '../../assets/css/Leaves/leaves.css'
import Header from '../Header/header'
import LeftMenu from '../NavBar/leftmenu'
import * as leaveActions from '../../store/actions/leaveActions'
import { Redirect } from 'react-router-dom'
import $ from "jquery"
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment'
import Footer from '../Footer/footer'

class Leave extends Component {
    constructor(props) {
        super(props);
        this.state = {
            leaveType: '',
            fromdate: new Date(),
            todate: new Date(),
            reason: '',
            isApplied: false,
            isApplied: false,
            error_leaveType: '',
            error_fromdate: '',
            error_todate: '',
            error_reason: '',
            errors: {}
        };
        this.onChangeLeaveType = this.onChangeLeaveType.bind(this);
        this.onChangeFrom = this.onChangeFrom.bind(this);
        this.onChangeTo = this.onChangeTo.bind(this);
        this.onChangeReason = this.onChangeReason.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    // componentDidMount = () => {
    //     var self = this;
    //     $('#fromDate').datepicker({
    //         format: "dd/mm/yyyy"
    //     }).on("changeDate", function (e) {
    //         console.log(self.state.msg);
    //     });
    // }
//WARNING! To be deprecated in React v17. Use componentDidMount instead.
componentWillMount() {
    var login_time = localStorage.getItem("time")
		var present_time = Date.now()
		var time =  present_time -login_time 
		console.log(time)
		if(time>=60*60*1000){
		  alert("Please Login again")
		  localStorage.removeItem("token")
		  window.location.href = '/'
		}
}

    onChangeLeaveType(e) {
        this.setState({ leaveType: e.target.value },()=>{
            this.validateleaveType()
        })
    }

    onChangeFrom(e) {
        this.setState({ fromdate: e });
        var date = this.state.fromdate.getDate();
        var month = this.state.fromdate.getMonth();
        var year = this.state.fromdate.getFullYear();
        console.log(date, month + 1, year);
    }

    onChangeTo(e) {
        this.setState({ todate: e });
    }

    onChangeReason(e) {
        this.setState({ reason: e.target.value }, () => {
            this.validateReason()
        });
    }

    onChangeUser(e) {
        this.setState({ username: e.target.value }, () => {
            this.validateUser()
        });
    }
    validateleaveType = () => {
        const { leaveType } = this.state;
        this.setState({
            error_leaveType:
            (leaveType=="sick" || leaveType== "casual"|| leaveType!= "--select--") ? null : 'Please Select',
        });
    }
    validateReason = () => {
        const { reason } = this.state;
        this.setState({
            error_reason:
                reason.length > 3 && reason.length <= 1000 ? null : 'Length should be in the range of 3 to 1000',
        });
    }

    onSubmit(e) {
        if(this.state.error_leaveType==null&&this.state.error_reason==null){
        var newFromDate = this.state.fromdate.getDate() + '-' + this.state.fromdate.getMonth() + 1 + '-' + this.state.fromdate.getFullYear();
        console.log("new from date", newFromDate);
        var toDate = this.state.todate.getDate() + '-' + this.state.todate.getMonth() + 1 + '-' + this.state.todate.getFullYear();
        e.preventDefault();
        const data = {
            employeeId:localStorage.getItem('userId'),
            leaveType: this.state.leaveType,
            fromdate: newFromDate,
            todate: toDate,
            reason: this.state.reason
        }
        this.props.leaveActions.createLeave(data);
        this.setState({
            isSubmitted: true
        })}
        else {
            alert("Fields must not be empty")
        }
    }


    render() {
        if (localStorage.getItem('token') == null) {
            return (<Redirect to="/" />)
        }
        if (this.props.isApplied == 200) {
            alert("Applied Leave Succefully");
            window.location.href="/salaryDetails";
        }
        if (this.props.isApplied == 201) {
            alert("No: of leaves exceeded...!!!\nMax leaves are 6")
            window.location.href="/salaryDetails";
        } 
        return (
            <div>
                <Header />
                <div className="row">
                    <div className="col-sm-3">
                        <LeftMenu />
                    </div>
                    <div class=" col-sm-9 register">
                        <div class="col-sm-12 register-right">
                            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                                <li class="nav-item">
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <h3 class="register-heading">Apply for a Leave</h3>
                                    <div class="row register-form">
                                        <div class="col-md-24">
                                            <div class="form-group">
                                                <label>Leave Type <span style={{ color: "red" }}>*&nbsp;&nbsp;&nbsp;</span></label>
                                                <br /> <select name="leaveType" type="type" onChange={this.onChangeLeaveType} >
                                                    <option value=""></option>
                                                    <option value="sick">sick</option>
                                                    <option value="casual">casual</option>
                                                </select>
                                                
                                                {/* <br/><input type = "search" list="leaveType" placeholder="Leave Type" onChange={this.onChangeLeaveType}/>
                                                <datalist id="leaveType">
                                                <option value="sick"></option>
                                                <option value="casual"></option></datalist> */}
                                                
                                            </div>
                                            <small id="headingHelp" className="form-text text-muted" />
                                            <div className='invalid-feedback'>{this.state.error_leaveType}</div>
                                        </div>


                                        <div class="form-group">
                                            <label>From Date: <span style={{ color: "red" }}>*&nbsp;&nbsp;&nbsp;</span></label>
                                            <td>
                                                <DatePicker
                                                    selected={this.state.fromdate}
                                                    onChange={this.onChangeFrom}
                                                    showDisabledMonthNavigation
                                                    value={this.props.fromdate}
                                                    name="fromdate" />
                                            </td>
                                        </div>

                                        <div class="form-group">
                                            <label>To Date: <span style={{ color: "red" }}>*&nbsp;&nbsp;&nbsp;</span></label>
                                            <td>
                                                <DatePicker
                                                    selected={this.state.todate}
                                                    selectsStart
                                                    todate={this.state.todate}
                                                    onChange={this.onChangeTo}
                                                    value={this.props.todate}
                                                    name="todate"
                                                    minDate={this.state.fromdate}
                                                />
                                            </td>
                                        </div>


                                        <div class="form-group">
                                            <label>Reason: <span style={{ color: "red" }}>*&nbsp;&nbsp;&nbsp;</span></label>
                                            <td><input type="text" className={`form-control ${this.state.error_reason ? 'is-invalid' : ''}`} placeholder="reason" required value={this.props.reason} onChange={this.onChangeReason}></input>
                                                <small id="headingHelp" className="form-text text-muted" />
                                                <div className='invalid-feedback'>{this.state.error_reason}</div></td>
                                        </div>
                                        <input type="button" class="btnRegister" value="ApplyLeave" required onClick={this.onSubmit} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-12 " style={{ marginTop: "0px" }}>
                    <Footer />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    debugger;
    console.log(state)
    return {
        isApplied: state.leaveReducer.isApplied,
        // leave_statuscode : state.leaveReducer.leave_statuscode
    }
}
function mapDispatchToProps(dispatch) {
    debugger;
    return {
        leaveActions: bindActionCreators(leaveActions, dispatch),
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(Leave)