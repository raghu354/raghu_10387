import React, { Component } from 'react';
import Header               from '../Header/header'
import NavBar               from '../NavBar/leftmenu'
import Footer               from '../Footer/footer'
import { Redirect }         from 'react-router-dom'

class Dashboard extends Component {
  componentWillMount() {
    var login_time = localStorage.getItem("time")
    var present_time = Date.now()
    var time =  present_time -login_time 
    console.log(time)
    if(time>=60*60*1000){
      alert("Please Login again")
      localStorage.removeItem("token")
      window.location.href = '/'
    }
  }

  render() {
    if(localStorage.getItem('token') == null){
      return (<Redirect to="/"/>)
    }
    return (
      <div className="App">
        <Header/>
        <div className="col-sm-3">
          <NavBar/>
        </div>
       
        <div className="col-sm-12 fixed-bottom" style={{marginTop:"0px"}}>
          <Footer/>
        </div>
      </div>
    );
  }
}

export default Dashboard;
