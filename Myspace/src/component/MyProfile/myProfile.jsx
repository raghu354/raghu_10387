import React, { Component }         from 'react'
import Avatar                       from '../../assets/images/img.png'
import Header                       from '../Header/header'
import LeftMenu                     from '../NavBar/leftmenu'
import                                   '../../assets/css/Profile/myProfile.css'
import { Link }                     from 'react-router-dom'
import { connect }                  from 'react-redux'
import * as authActions             from '../../store/actions/authActions'
import { bindActionCreators }       from 'redux';
import { Redirect }                 from 'react-router-dom'

const details = {
    borderRadius: "10px",
    fontSize: "1.0vw",
    padding: "2px",
    marginTop: "-50px"
}



class MyProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            myProfile: [],
            isLoaded: false
        }
    }

    componentWillMount() {
        var login_time = localStorage.getItem("time")
		var present_time = Date.now()
		var time =  present_time -login_time 
		console.log(time)
		if(time>=60*60*1000){
		  alert("Please Login again")
		  localStorage.removeItem("token")
		  window.location.href = '/'
        }
        else{ const id = localStorage.getItem('userId');
        this.props.authActions.fetchMyProfile(id);}
       
    }

    componentWillReceiveProps(newProps) {
        if (newProps.profile_data) {
            this.setState({
                myProfile: newProps.profile_data
            })
        }
    }

    render() {
        if(localStorage.getItem('token') == null){
            return (<Redirect to="/"/>)
        }
        return (
            <div>
                <Header />
                <div className="row">
                    <div className="col-sm-3">
                        <LeftMenu />
                    </div>
                    <div className="col-sm-9"> 
                        <div className="col-sm-12">
                            <h5>Employee Info</h5>
                        </div>
                    <div className="col-sm-12 row">
                        <div id="vcard" className="col-sm-10">
                            <div id="card-content">
                                <div id="profile">
                                    <span class="avatar">
                                        <img class="typicons-user icon" style={{ width: "50px", height: "50px", borderRadius: "50%" }} src={Avatar} />
                                        <span class="info">
                                            {this.state.myProfile.name}({this.state.myProfile.employeeId})<br />
                                            {this.state.myProfile.companyEmailId}<br />
                                            {this.state.myProfile.primaryMobileNumber}
                                        </span>
                                    </span>
                                </div>
                                <div id="options" >
                                    <center><ul>
                                        <li><div class="box"><span class="fa faicon fa-street-view "></span><p>JOB STATUS</p><div class="overlay"><span id="plus">{this.state.myProfile.jobStatus}</span></div></div></li>
                                        <li><div class="box"><span class="fa faicon fa-user"></span><p>EMPLOYEE TYPE</p><div class="overlay"><span id="plus">{this.state.myProfile.employmentType}</span></div></div></li>
                                        <li><div class="box"><span class="fa faicon fa-map-marker"></span><p>WORK LOCATION</p><div class="overlay"><span id="plus">{this.state.myProfile.workLocation}</span></div></div></li>
                                        <li><div class="box"><span class="fa faicon fa-desktop"></span><p>DOMAIN</p><div class="overlay"><span id="plus">{this.state.myProfile.designation}</span></div></div>
                                        </li>
                                    </ul></center>
                                </div>
								
                            </div>
							
                        </div>
						<div id="side-bar" className="col-sm-2">
                                <ul>
                                    <li><span class="typicons-list icon"></span><Link to={`/editProfile/${localStorage.getItem('userId')}`}>EditProfile</Link></li>
                                    <li><span class="typicons-list icon"></span>PROJECTS</li>
                                    <li><span class="typicons-spanner icon"></span>SKILLS</li>
                                </ul>
                            </div>
                    </div>
                </div>
                </div>
                </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        profile_loaded: state.loginReducer.my_profile,
        profile_data: state.loginReducer.user_details
    }
}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyProfile)