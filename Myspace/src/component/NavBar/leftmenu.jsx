import React, { Component } from 'react';
import $ 					from 'jquery';
import {Link} 				from 'react-router-dom'
import 							 'bootstrap/dist/css/bootstrap.css';



const vert = { 
    background: "linear-gradient(to right,rgba(18,86,141,0.5),  white)",
    borderRadius: "25px",
    borderColor: "white",
    padding: "4px",
    width: "100%",
    color: "black",
    marginTop:"2px"
}

const open= {
    display:"none",
    borderRadius:"10px",
    width:"100%",
    background: "linear-gradient(rgba(18,86,141,0.5), 70%, white)"
}


        

class LeftMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    _toggleDiv(id) {
        let value = this.state.opened;
        if (id === 1 || value === 1) {
            $(this.refs['empopen']).slideToggle();
        }
        if (id === 2 || value === 2) {
            $(this.refs['pefopen']).slideToggle();
        }
        if (id === 3 || value === 3) {
            $(this.refs['awardsopen']).slideToggle();
        }
        if (id === 4 || value === 4) {
            $(this.refs['libraryopen']).slideToggle();
        }
        if (id === 5 || value === 5) {
            $(this.refs['clientopen']).slideToggle();
        }
        if (id === 6 || value === 6) {
            $(this.refs['docopen']).slideToggle();
        }
        this.setState({ opened: id });
    }

    componentWillMount(){
        const id = localStorage.getItem('userId');
        console.log("empID",id);
    }
 
    render() {
        return (

                <div className="" style={{background: "linear-gradient(rgba(18,86,141,0.5), 70%, white)",borderRadius:"20px"}}>
                    <div className="list" onClick={this._toggleDiv.bind(this, 1)}><button style={vert}><i class="fas fa-chevron-circle-right" style={{float:"left"}}></i><Link to={`/myProfile/${localStorage.getItem('userId')}`}>Employee Info</Link></button></div>
                    <div className="list" onClick={this._toggleDiv.bind(this, 3)}><button style={vert}><i class="fas fa-chevron-circle-right" style={{float:"left"}}></i><Link to="/culturalEvents">Cultural Events</Link></button></div>                    
                    <div className="list" onClick={this._toggleDiv.bind(this, 3)}><button style={vert}><i class="fas fa-chevron-circle-right" style={{float:"left"}}></i><Link to={`/salaryDetails/${localStorage.getItem('userId')}`}>Salary Details</Link></button></div>
                    <div className="list" onClick={this._toggleDiv.bind(this, 4)}><button style={vert}><i class="fas fa-chevron-circle-right" style={{float:"left"}}></i><Link to="/training">Training Sessions</Link></button></div>
                    <div className="list" onClick={this._toggleDiv.bind(this, 4)}><button style={vert}><i class="fas fa-chevron-circle-right" style={{float:"left"}}></i><Link to="/applyLeave">Apply for Leave</Link></button></div>
                    <div className="list" onClick={this._toggleDiv.bind(this, 5)}><button style={vert}><i class="fas fa-chevron-circle-right" style={{float:"left"}}></i><Link to="/holiday">Holidays</Link></button></div>
                    
                </div>
                 

        );
    }
}

export default LeftMenu;