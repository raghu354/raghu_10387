import React, { Component } from "react";
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import '../../assets/css/SalaryDetails/salaryDetails.css';
import * as salaryActions from '../../store/actions/salaryActions';
import Footer from '../Footer/footer';
import Header from '../Header/header';
import LeftMenu from '../NavBar/leftmenu';
import { MagicSpinner  } from "react-spinners-kit";

class SalaryDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            previousLeaves: ""
        }
    }

    componentWillMount() {
        var login_time = localStorage.getItem("time")
		var present_time = Date.now()
		var time =  present_time -login_time 
		console.log(time)
		if(time>=60*60*1000){
		  alert("Please Login again")
		  localStorage.removeItem("token")
		  window.location.href = '/'
        }
        else{

        const id = localStorage.getItem('userId');
        console.log(id);
        this.props.salaryActions.getSalaryDetails(id);
        }
    }

    // componentDidMount(){
    //     console.log("Dis mount",this.props.salary)
    //     if(this.props.salary){
    //         if(this.props.salary.numberOfLeaves>this.props.salary.totalNoOfLeaves){
    //             this.setState({previousLeaves : this.props.salary.numberOfLeaves-this.props.salary.totalNoOfLeaves})

    //         }
    //         else{
    //             this.setState({previousLeaves : this.props.salary.totalNoOfLeaves-this.props.salary.numberOfLeaves})
    //         }
    //     }
    // }

    render() {

        if (localStorage.getItem('token') == null) {
            return (<Redirect to="/" />)
        }
        console.log("Prev leaves", this.state.previousLeaves)
        return (

            <div>
                    <Header />
                    <div className="row">
                        <div className="col-sm-3">
                            <LeftMenu />
                        </div>
                        <div class="col-sm-8">
                            {
                                (this.props.salary) ?
                                    <div class="">
                                        <div>
                                            <div key={this.props.salary.employeeId}>
                                                <div class='card-container  '>
                                                    <div class='card-heading' >
                                                        <h3>Recent Leaves</h3>
                                                        <table class="tablesalary table-borderless  ">
                                                            <thead>
                                                                <tr>
                                                                    <th>Employee Id</th>
                                                                    <th>LeaveType</th>
                                                                    <th>Reason</th>
                                                                    <th>FromDate</th>
                                                                    <th>ToDate</th>
                                                                    <th>No.of Leaves</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>{this.props.salary.employeeId}</td>
                                                                    <td>{this.props.salary.leaveType}</td>
                                                                    <td>{this.props.salary.reason}</td>
                                                                    <td>{this.props.salary.fromdate}</td>
                                                                    <td>{this.props.salary.todate}</td>
                                                                    <td>{this.props.salary.numberOfLeaves}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class='card-container  '>
                                                    <div class='card-heading' >
                                                        <div style={{ backgroundColor: "#78b5ba" }}> <h3 style={{ color: "white" }}>Salary Details</h3></div>
                                                        <table class="tablesalary table-borderless" >
                                                            <tbody>
                                                                <tr>
                                                                    <td ><b>Basic Pay</b></td>
                                                                    <td>{this.props.salary.basicPay}</td>
                                                                    <td><b>Provident Fund</b></td>
                                                                    <td>{this.props.salary.providentFund}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>House Rent Allowances</b></td>
                                                                    <td>{this.props.salary.hra}</td>
                                                                    <td> <b>Total No. of Leaves</b></td>
                                                                    <td> {this.props.salary.totalNoOfLeaves}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Statutory Bonus</b></td>
                                                                    <td>{this.props.salary.statutoryBonus}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <hr />
                                                    </div>
                                                    <div>
                                                        <table class="tablesalary table-borderless">
                                                            <tr>
                                                                <td><b>Total Salary</b></td>
                                                                <td>15000</td>
                                                                <td><b>Employee Salary</b></td>
                                                                <td>{this.props.salary.totalsalary}</td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        </div> :
                        <div>Loading
                                <MagicSpinner 
                                    size={40}
                                    color="black"

                                >Lodaing</MagicSpinner >
                            </div>

                            }
               
                            </div>
                            <div className="col-sm-12 " style={{ marginTop: "0px" }}>
                                <Footer />
                            </div>
                        </div>
                        </div>
                        )
                    }
                }
                
function mapStateToProps(state) {
    debugger;
    if (state.salaryReducer.salary) {
        if (state.salaryReducer.salary.numberOfLeaves > state.salaryReducer.salary.totalNoOfLeaves) {
                            this.setState({ previousLeaves: state.salaryReducer.salary.numberOfLeaves - this.props.salary.totalNoOfLeaves })

                        }

                        }
                        console.log(state.salaryReducer.salary);
    return {
                            salar: state.salaryReducer.salary
                    };
                }
                
function mapDispatchToprops(dispatch) {
    return {
                            salaryActions: bindActionCreators(salaryActions, dispatch)
                    };
                }
                
export default connect(mapStateToProps, mapDispatchToprops)(SalaryDetails);