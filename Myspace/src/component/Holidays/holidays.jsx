import React, { Component } from 'react';
import $ from "jquery";
import '../../assets/css/Holidays/holiday.css'
import Header from '../Header/header';
import LeftMenu from '../NavBar/leftmenu';
import { Redirect } from 'react-router-dom'
import Footer from '../Footer/footer';

class Holiday extends Component {
    //WARNING! To be deprecated in React v17. Use componentDidMount instead.
    componentWillMount() {
        var login_time = localStorage.getItem("time")
		var present_time = Date.now()
		var time =  present_time -login_time 
		console.log(time)
		if(time>=60*60*1000){
		  alert("Please Login again")
		  localStorage.removeItem("token")
		  window.location.href = '/'
		}
    }
    componentDidMount() {
        $(document).ready(function () {
            $("button").click(function () {
                $(".2018").toggle();
            });
        });

        $("#nav a").click(function (e) {
            e.preventDefault();
            $(".toggle1").hide();
            var toShow = $(this).attr('href');
            $(toShow).show();
        });
    }
    render() {
        if(localStorage.getItem('token') == null){
            return (<Redirect to="/"/>)
          }
        return (
            <div>

                <Header />
                <div className="row">
                    <div className="col-sm-3">
                        <LeftMenu />
                    </div>
                    <div class="holiday col-sm-9">

                        <div  >
                            <button type="button" id="2019" class="btn btn-info holidaybtn">CLICK FOR HOLIDAYS</button>
                            <br />
                            <br />

                        </div>
                        <div class="2018" style={{ display: "none" }}>
                            <ul class="nav nav-pills">
                                <li class="active "><a class="hol" href="#hyd">Hyderabad</a></li>
                                <li><a class="hol" href="#banglore" >Banglore</a></li>
                                <li><a  class="hol" href="#Us">United states</a></li>
                            </ul><br />
                            <div class="holidaytbl toggle1" id="hyd" >
                                <h3 class="h3holiday">Hyderabad and Visakhapatnam  Holidays</h3>
                                <table class="table 2019 table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th >Month</th>
                                            <th >Date</th>
                                            <th >Day</th>
                                            <th >Occasion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>January</td>
                                            <td>01-01-2019</td>
                                            <td>Tuesday</td>
                                            <td>New Year Day</td>
                                        </tr>
                                        <tr>
                                            <td>January</td>
                                            <td>15-01-2019</td>
                                            <td>Tuesday</td>
                                            <td>Makara Sankranthi</td>
                                        </tr>
                                        <tr>
                                            <td>March</td>
                                            <td>20-03-2019</td>
                                            <td>Wednesday</td>
                                            <td>Holi</td>
                                        </tr>
                                        <tr>
                                            <td>June</td>
                                            <td>05-06-2019</td>
                                            <td>Wednesday</td>
                                            <td>Ramzan</td>
                                        </tr>
                                        <tr>
                                            <td >August</td>
                                            <td>15-08-2019</td>
                                            <td>Thursday</td>
                                            <td>Independence Day</td>
                                        </tr>
                                        <tr>
                                            <td >September</td>
                                            <td>02-09-2019</td>
                                            <td>Monday</td>
                                            <td>Ganesh Chaturthi</td>
                                        </tr>
                                        <tr>
                                            <td>October</td>
                                            <td>08-10-2019</td>
                                            <td>Tuesday</td>
                                            <td>Vijaya DasamI</td>
                                        </tr>
                                        <tr>
                                            <td>December</td>
                                            <td>25-12-2019</td>
                                            <td>Wednesday</td>
                                            <td>Christmas</td>
                                        </tr>
                                        <tr>
                                            <td >=</td>
                                            <td colSpan="3">Any day of your choice including birthday & wedding anniversary</td>
                                        </tr>
                                        <tr>
                                            <td >=</td>
                                            <td colSpan="3">Any day that is not included in this list but forced by the external factors during the calendar year, 2019</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="holidaytbl toggle1" id="banglore">
                                <h3 class="h3holiday" >Banglore Holidays</h3>
                                <table class="table 2019 table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th >Month</th>
                                            <th >Date</th>
                                            <th >Day</th>
                                            <th >Occasion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>January</td>
                                            <td>01-01-2019</td>
                                            <td>Tuesday</td>
                                            <td>New Year Day</td>
                                        </tr>
                                        <tr>
                                            <td>January</td>
                                            <td>15-01-2019</td>
                                            <td>Tuesday</td>
                                            <td>Makara Sankranthi</td>
                                        </tr>
                                        <tr>
                                            <td>March</td>
                                            <td>20-03-2019</td>
                                            <td>Wednesday</td>
                                            <td>Holi</td>
                                        </tr>
                                        <tr>
                                            <td>June</td>
                                            <td>05-06-2019</td>
                                            <td>Wednesday</td>
                                            <td>Ramzan</td>
                                        </tr>
                                        <tr>
                                            <td >August</td>
                                            <td>15-08-2019</td>
                                            <td>Thursday</td>
                                            <td>Independence Day</td>
                                        </tr>
                                        <tr>
                                            <td >September</td>
                                            <td>02-09-2019</td>
                                            <td>Monday</td>
                                            <td>Ganesh Chaturthi</td>
                                        </tr>
                                        <tr>
                                            <td>October</td>
                                            <td>08-10-2019</td>
                                            <td>Tuesday</td>
                                            <td>Vijaya DasamI</td>
                                        </tr>
                                        <tr>
                                            <td>November</td>
                                            <td>01-11-2019</td>
                                            <td>Friday</td>
                                            <td>Formation Day</td>
                                        </tr>
                                        <tr>
                                            <td>December</td>
                                            <td>25-12-2019</td>
                                            <td>Wednesday</td>
                                            <td>Christmas</td>
                                        </tr>
                                        <tr>
                                            <td >=</td>
                                            <td colSpan="3">Any day of your choice including birthday & wedding anniversary</td>
                                        </tr>
                                        <tr>
                                            <td >=</td>
                                            <td colSpan="3">Any day that is not included in this list but forced by the external factors during the calendar year, 2019</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="holidaytbl toggle1" id="Us">
                                <h3 class="h3holiday">Us Holidays</h3>
                                <table class="table 2019 table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th >Month</th>
                                            <th >Date</th>
                                            <th >Day</th>
                                            <th >Occasion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>January</td>
                                            <td>01-01-2019</td>
                                            <td>Tuesday</td>
                                            <td>New Year Day</td>
                                        </tr>
                                        <tr>
                                            <td>May</td>
                                            <td>27-05-2019</td>
                                            <td>Monday</td>
                                            <td>Memorial Day</td>
                                        </tr>
                                        <tr>
                                            <td>July</td>
                                            <td>04-07-2019</td>
                                            <td>Thursday</td>
                                            <td>Independence Day</td>
                                        </tr>
                                        <tr>
                                            <td>September</td>
                                            <td>02-09-2019</td>
                                            <td>Monday</td>
                                            <td>Labor day</td>
                                        </tr>
                                        <tr>
                                            <td >November</td>
                                            <td>11-11-2019</td>
                                            <td>Monday</td>
                                            <td>Veterans Day</td>
                                        </tr>
                                        <tr>
                                            <td >November</td>
                                            <td>28-11-2019</td>
                                            <td>Thursday</td>
                                            <td>Thanksgiving Day</td>
                                        </tr>
                                        <tr>
                                            <td>November</td>
                                            <td>29-11-2019</td>
                                            <td>Friday</td>
                                            <td>Day After Thanksgiving</td>
                                        </tr>
                                        <tr>
                                            <td>December</td>
                                            <td>25-12-2019</td>
                                            <td>Wednesday</td>
                                            <td>Christmas</td>
                                        </tr>
                                        <tr>
                                            <td>December</td>
                                            <td>26-12-2019</td>
                                            <td>Thursday</td>
                                            <td>Day After Christmas</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-12" style={{ marginTop: "0px" }}>
                    <Footer />
                </div>
            </div>
        )
    }
}

export default Holiday;