import React from 'react';
import Header from '../Header/header'
import LeftMenu from '../NavBar/leftmenu';
import Footer from '../Footer/footer';
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import * as trainingActions from '../../store/actions/trainingActions'
import { bindActionCreators } from 'redux';
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar/dist/fullcalendar.css';


class Training extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      start: "",
      end: "",
      event: []
    }
  }
  componentWillReceiveProps(new_props) {
    if (new_props.profile_loaded) {
      console.log(new_props.profile_loaded);
      this.setState({ event: new_props.profile_loaded })
      console.log("Event", this.state.event)
    }
  }
//WARNING! To be deprecated in React v17. Use componentDidMount instead.
componentWillMount() {
  var login_time = localStorage.getItem("time")
  var present_time = Date.now()
  var time =  present_time -login_time 
  console.log(time)
  if(time>=60*60*1000){
    alert("Please Login again")
    localStorage.removeItem("token")
    window.location.href = '/'
      }

  }
  componentDidMount() {
    debugger
    this.props.trainingActions.fetchTime();
  }

  render() {
  


    if (localStorage.getItem('token') == null) {
      return (<Redirect to="/" />)
    }
    return (
      <div>
        <Header />
        <div className="row">
          <div className="col-sm-3">
            <LeftMenu />
          </div>
          <div className="col-sm-9">
            <FullCalendar
              id="your-custom-ID"
              header={{
                left: 'prev,next today myCustomButton',
                center: 'title',
                right: 'month,basicWeek,basicDay'
              }}

              timeFormat='H(:mm)a'
              eventColor='#378006'
              displayEventStart={true}
              navLinks={true}
              eventLimit={true}
              events={this.state.event}
              eventClick={function (event) {
              }
              }
            />
          </div>
        </div>
        <div className="col-sm-12 " style={{ marginTop: "0px" }}>
          <Footer />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state.trainingReducer.events)
  return {
    profile_loaded: state.trainingReducer.events,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    trainingActions: bindActionCreators(trainingActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Training)