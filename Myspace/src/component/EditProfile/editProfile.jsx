import React, { Component } from 'react'
import * as authActions from '../../store/actions/authActions'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import Header from '../Header/header'
import LeftMenu from '../NavBar/leftmenu'
import '../../assets/css/EditProfile/Edit.css'
import Footer from '../Footer/footer';

class EditProfile extends Component {

	constructor(props) {
		super(props);
		this.state = {
			alternativeMobileNumber: '',
			competency: '',
			permanentLocation: '',
			personalEmailId: '',
			primaryMobileNumber: '',
			workLocation: '',
			workPlacePhoneNumber: '',
			myProfile: [],
			isLoaded: false,
			is_updated_data: true,
			error_alt_MobileNumber: '',
			error_competency: '',
			error_per_Address: '',
			error_email: '',
			error_pri_mobile: '',
			error_workplace_num: '',
			error_location: '',
		}
		this.onChangealternativeMobileNumber = this.onChangealternativeMobileNumber.bind(this);
		this.onChange_competency = this.onChange_competency.bind(this)
		this.onChange_per_Address = this.onChange_per_Address.bind(this);
		this.onChange_email = this.onChange_email.bind(this)
		this.onChange_pri_mobile = this.onChange_pri_mobile.bind(this);
		this.onChange_workplace_num = this.onChange_workplace_num.bind(this)
		this.onChange_location = this.onChange_location.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	onChangealternativeMobileNumber(e) {
		this.setState({ alternativeMobileNumber: e.target.value }, () => {
			this.validate_alt_MobileNumber();
		}
		);
	}
	onChange_competency(e) {
		this.setState({ competency: e.target.value }, () => {
			this.validate_competency();
		}
		);
	}
	onChange_per_Address(e) {
		this.setState({ permanentLocation: e.target.value }, () => {
			this.validate_per_Address();
		}
		);
	}
	onChange_email(e) {
		this.setState({ personalEmailId: e.target.value }, () => {
			this.validate_email();
		}
		);
	}
	onChange_pri_mobile(e) {
		this.setState({ primaryMobileNumber: e.target.value }, () => {
			this.validate_pri_mobile();
		}
		);
	}
	onChange_workplace_num(e) {
		this.setState({ workPlacePhoneNumber: e.target.value }, () => {
			this.validate_workplace_num();
		}
		);
	}
	onChange_location(e) {
		this.setState({ workLocation: e.target.value }, () => {
			this.validate_location();
		}
		);
	}
	validate_alt_MobileNumber = () => {
		const { alternativeMobileNumber } = this.state;
		this.setState({
			error_alt_MobileNumber:
				(/^[6-9]{1}[0-9]{9}$/).test(alternativeMobileNumber)||alternativeMobileNumber.length==0 ? null : 'Invalid Mobile Number',
		});
	}
	validate_competency = () => {
		const { competency } = this.state;
		this.setState({
			error_competency:
			( competency.length <= 25)&&((/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/).test(competency)||(/^[a-zA-Z]*$/).test(competency))? null : 'Length should be in the range of 0 to 25',		});
	}
	validate_per_Address = () => {
		const { permanentLocation } = this.state;
		this.setState({
			error_per_Address:
			( permanentLocation.length <= 25)&&((/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/).test(permanentLocation)||(/^[a-zA-Z]*$/).test(permanentLocation))? null : 'Length should be in the range of 0 to 25'});
	}
	validate_email = () => {
		const { personalEmailId } = this.state;
		this.setState({
			error_email:
			personalEmailId.length > 6 && personalEmailId.length < 40 && ((/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/).test(personalEmailId)) ||  ((/^[a-z0-9](\.?[a-z0-9]){5,}@y(oogle)?ahoo\.com$/).test(personalEmailId))||((/^[a-z0-9](\.?[a-z0-9]){5,}@y(oogle)?ahoo\.in$/).test(personalEmailId))||((/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.in$/).test(personalEmailId)) ? null : 'Invalid Email',})
	}
	validate_pri_mobile = () => {
		const { primaryMobileNumber } = this.state;
		this.setState({
			error_pri_mobile:
				(/^[6-9]{1}[0-9]{9}$/).test(primaryMobileNumber) ? null : 'Invalid Mobile Number',
		});
	}

	validate_workplace_num = () => {
		const { workPlacePhoneNumber } = this.state;
		this.setState({
			error_workplace_num:
				(/^[6-9]{1}[0-9]{9}$/).test(workPlacePhoneNumber) ? null : 'Invalid Mobile Number',
		});
	}

	validate_location = () => {
		const { workLocation } = this.state;
		this.setState({
			error_location:
			( workLocation.length <= 25)&&((/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/).test(workLocation)||(/^[a-zA-Z]*$/).test(workLocation))? null : 'Length should be in the range of 0 to 25'});
	}


	onSubmit(e) {
		if(this.state.error_alt_MobileNumber==null||this.state.error_competency==null||this.state.error_per_Address==null||this.state.error_email==null||this.state.error_pri_mobile==null||this.state.error_workplace_num==null||this.state.error_location==null){
			const id = this.props.match.params.id;
		e.preventDefault();
		const data = {
			alternativeMobileNumber: this.state.alternativeMobileNumber,
			competency: this.state.competency,
			permanentLocation: this.state.permanentLocation,
			personalEmailId: this.state.personalEmailId,
			primaryMobileNumber: this.state.primaryMobileNumber,
			workLocation: this.state.workLocation,
			workPlacePhoneNumber: this.state.workPlacePhoneNumber,
		}
		this.props.authActions.editProfile(id, data);
		this.setState({
			isSubmitted: true
		})
		}
		else (alert("Invalid"))
	}
	componentDidMount() {
	}

	componentWillMount() {
		
		var login_time = localStorage.getItem("time")
		var present_time = Date.now()
		var time =  present_time -login_time 
		console.log(time)
		if(time>=60*60*1000){
		  alert("Please Login again")
		  localStorage.removeItem("token")
		  window.location.href = '/'
		}
		else {
			const id = this.props.match.params.id;
		this.props.authActions.fetchMyProfile(id);
		}

	}

	componentWillReceiveProps(newProps) {
		if (newProps.profile_data) {
			this.setState({
				myProfile: newProps.profile_data,
				alternativeMobileNumber: newProps.profile_data.alternativeMobileNumber,
				competency: newProps.profile_data.competency,
				permanentLocation: newProps.profile_data.permanentLocation,
				personalEmailId: newProps.profile_data.personalEmailId,
				primaryMobileNumber: newProps.profile_data.primaryMobileNumber,
				workLocation: newProps.profile_data.workLocation,
				workPlacePhoneNumber: newProps.profile_data.workPlacePhoneNumber,
			})
		}
		
	}
	render() {
		if(this.props.edit_statuscode){
			console.log(this.props.edit_statuscode)
			alert("Updated Succefully")
			return (window.location.href = '/dashboard')	
		}
		return (
			<div>
				<Header />
				<div className="row">
					<div className="col-sm-3">
						<LeftMenu />
					</div>
					<div>
						<h4>Edit Form</h4>
						<div class="sub-main col-sm-9">

							<form onSubmit={this.onSubmit}>
								<div class="form-style-agile">
									<label>
										Alternate Mobile No:
								<i class="fas fa-user"></i>
									</label>
									<input type="type" placeholder="Alt mobile no." name="alternativeMobileNumber" className={`form-control ${this.state.error_alt_MobileNumber ? 'is-invalid' : ''}`} value={this.state.alternativeMobileNumber} onChange={this.onChangealternativeMobileNumber}></input>
									<small id="headingHelp" className="form-text text-muted" />
									<div className='invalid-feedback'>{this.state.error_alt_MobileNumber}</div>
								</div>

								<div class="form-style-agile">
									<label>
										Competency
								<i class="fas fa-unlock-alt"></i>
									</label>
									<input type="type" placeholder="Competency" name="competency" className={`form-control ${this.state.error_competency ? 'is-invalid' : ''}`} value={this.state.competency} onChange={this.onChange_competency}></input>
									<small id="headingHelp" className="form-text text-muted" />
									<div className='invalid-feedback'>{this.state.error_competency}</div>
								</div>

								<div class="form-style-agile">
									<label>
										Permenant Location
								<i class="fas fa-unlock-alt"></i>
									</label>
									<input className={`form-control ${this.state.error_per_Address ? 'is-invalid' : ''}`} placeholder="Permenant Location" name="permanentLocation" onChange={this.onChange_per_Address} value={this.state.permanentLocation} type="type" />
									<small id="headingHelp" className="form-text text-muted" />
									<div className='invalid-feedback'>{this.state.error_per_Address}</div>
								</div>

								<div class="form-style-agile">
									<label>
										Personal Email
								<i class="fas fa-unlock-alt"></i>
									</label>
									<input className={`form-control ${this.state.error_email ? 'is-invalid' : ''}`} placeholder="Personal Email" name="personalEmailId" onChange={this.onChange_email} value={this.state.personalEmailId} type="type" />
									<small id="headingHelp" className="form-text text-muted" />
									<div className='invalid-feedback'>{this.state.error_email}</div>
								</div>

								<div class="form-style-agile">
									<label>
										Primary Mobile Number
								<i class="fas fa-unlock-alt"></i>
									</label>
									<input className={`form-control ${this.state.error_pri_mobile ? 'is-invalid' : ''}`} placeholder="Primary Mobile Number" name="primaryMobileNumber" onChange={this.onChange_pri_mobile} value={this.state.primaryMobileNumber} type="type" />
									<small id="headingHelp" className="form-text text-muted" />
									<div className='invalid-feedback'>{this.state.error_pri_mobile}</div>
								</div>

								<div class="form-style-agile">
									<label>
										Work Location
								<i class="fas fa-unlock-alt"></i>
									</label>
									<input className={`form-control ${this.state.error_location ? 'is-invalid' : ''}`} placeholder="Work Location" name="workLocation" onChange={this.onChange_location} value={this.state.workLocation} type="type" />
									<small id="headingHelp" className="form-text text-muted" />
									<div className='invalid-feedback'>{this.state.error_location}</div>
								</div>

								<div class="form-style-agile">
									<label>
										Work Place Phone Number
								<i class="fas fa-unlock-alt"></i>
									</label>
									<input className={`form-control ${this.state.error_workplace_num ? 'is-invalid' : ''}`} placeholder="Work Place Phone Number" name="workPlacePhoneNumber" onChange={this.onChange_workplace_num} value={this.state.workPlacePhoneNumber} type="type" />
									<small id="headingHelp" className="form-text text-muted" />
									<div className='invalid-feedback'>{this.state.error_workplace_num}</div>
								</div>

								<input type="submit" value="Update" />
							</form>
						</div>
					</div>
				</div>
				<div className="col-sm-12" style={{ marginTop: "0px" }}>
                    <Footer />
                </div>
			</div>
		)
	}

}

function mapStateToProps(state) {
	console.log(state.loginReducer.edit_statuscode);
	return {
		profile_loaded: state.loginReducer.my_profile,
		profile_data: state.loginReducer.user_details,
		update_profile: state.loginReducer.update_profile,
		edit_statuscode:state.loginReducer.edit_statuscode
	}
}

function mapDispatchToProps(dispatch) {
	return {
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)