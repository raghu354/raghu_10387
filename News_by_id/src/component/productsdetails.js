import React,{Component} from 'react';


class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
               product : [],
               isLoaded : false 
         }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
            .then(response => response.json())
            .then(Data => {
                this.setState({
                    isLoaded: true,
                    product: Data
                })
            });
    }
    render() { 
        var { isLoaded, product } = this.state;

            if (!isLoaded) {
                return <div>Loading...</div>
            }
            return(
                <div className ="Product">
                            <h2>Product Details</h2>
                      <center><table style={{textAlign:"center",width:"100%"}}>
                          <tr style={{backgroundColor:"red",padding:"100px"}}>
                              <th style={{border:"solid 2px white",}} >product_id</th>
                              <th style={{border:"solid 2px white"}}>Product Name</th>
                              <th style={{border:"solid 2px white"}}>price</th>
                              <th style={{border:"solid 2px white"}}>Quantity</th>
                              <th style={{border:"solid 2px white"}}>Type</th>
                          </tr>
                          
                            {product.map( items => (
                        <tr>
                                <td style={{backgroundColor:"pink",border:"solid 2px white"}}>{items.product_id}</td> 
                                <td style={{backgroundColor:"aqua",border:"solid 2px white"}}>{items.productname}</td>  
                                <td style={{backgroundColor:"aqua",border:"solid 2px white"}}>{items.price} </td>
                                <td style={{backgroundColor:"aqua",border:"solid 2px white"}}>{items.quantity}</td>
                                <td style={{backgroundColor:"aqua",border:"solid 2px white"}}>{items.productType}</td>
                        </tr>
                            ))}
                            
                      </table></center> 
                </div> 
        );
    }
}
 
export default ProductList;