import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../store/actions/authActions';
import {Redirect} from 'react-router-dom';

const div = {
    backgroundImage: "url('http://getwallpapers.com/wallpaper/full/a/5/d/544750.jpg')",
    backgroundSize: "cover",
    width:"400px",
    height:"100%",
    borderradius: "25px",
}
const form = {
    width:"300px",
    textAlign:"left",
    color:"white"
}


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            email:"",
            password:"",
            isSubmitted:false
         }
        this.handleChange  =  this.handleChange.bind(this);
        this.doLogin  =  this.doLogin.bind(this)
    }
    handleChange  =  (e) => {
        this.setState({
            [e.target.name ]:  e.target.value
        })
        e.preventDefault();
    }
        doLogin = (e) =>{
            this.props.authActions.doLogin({
                email:this.state.email,
                password:this.state.password
            })
            this.setState({
                isSubmitted:true
            })
        }
    
    render() {
        if(this.state.isSubmitted){
             if(localStorage.getItem("jwt-token")&& this.props.isLoggedIn==true){
                 return <Redirect to="/admin/dashboard"/>
             }   
        }
         

             
         
        return (

            <div style={div} className= "container"> 
                  <center><form onSubmit = {this.doLogin} method = "post" style={form} >
                        <div class="form-group">
                        <label><b>Email:</b></label>
                        <input type="email" className="form-control"  placeholder="Enter email" name="email" value= {this.state.email} onChange={this.handleChange}/>
                        </div>
                        <div class="form-group">
                        <label><b>Password:</b></label>
                        <input type="password" className="form-control" placeholder="Enter password" name="password" value= {this.state.password} onChange={this.handleChange}/>
                        </div>
                        <button type="submit" className="btn btn-danger">Submit</button>
                    </form></center>
            </div>
         );
    }
}

function mapStateToProps(state){
    return {isLoggedIn : state.isLoggedIn}
}
function mapDispatchToProps(dispatch){
    return{
        authActions:bindActionCreators(authActions , dispatch),
    };
}
 
export default connect(mapStateToProps , mapDispatchToProps)(Login);