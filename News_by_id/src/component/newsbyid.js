import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsActions';



class Newsbyid extends Component {
    constructor(props) {
        super(props);
        this.state={
            newsItems:{}
        }
    
    }
    componentWillMount(){
        this.props.newsActions.fetchNewsbyid(this.props.match.params.id);
    }
    componentWillReceiveProps(newsProps){
        debugger;
        if(newsProps.newsItems1){
            this.setState({
                newsItems :newsProps.newsItems1
            })
        }
       
    }
    render(){
        return(
           <div> 
              <h2>{this.state.newsItems.timestamp}</h2>
           </div>
        );
    }
 }
 function mapStateToProps(state){
 
     return{
         newsItems1: state.newsbyid,
     };
 }
 function mapDispatchToProps(dispatch){
     return{
         newsActions:bindActionCreators(newsActions , dispatch),
     };
 }
export default connect(mapStateToProps , mapDispatchToProps)(Newsbyid);
