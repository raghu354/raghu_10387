import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsActions';
import {Link} from 'react-router-dom';

class News extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        this.props.newsActions.fetchNews();
    }
    render(){
        return(
            <div>
                {                  
                    (this.props.newsItems)
                        ? <div class="container">
                            {this.props.newsItems.map(item => (
                                <div key={item._id}>
                                <Link to={'/news/'+item._id}><li key={item._id}>
                                    {item._id}
                                    </li></Link>
                                    {/* <h3 align="center" style={{color:"green",textDecoration:"underline"}}>{item.heading}</h3>
                                    <h5 style={{fontFamily:"georgia",color:"red"}}>{item.brief}</h5>
                                    <p style={{fontFamily:"arial",textAlign:"justify"}}>{item.body}</p>
                                    <p style={{fontFamily:"arial",color:"blue"}}>TimeStamp : <span style={{color:"black"}}>{item.timestamp}</span></p>
                                    <p style={{fontFamily:"arial",color:"blue"}}>Tags : <span style={{color:"black"}}>{item.tags}</span></p>
                                    <p style={{fontFamily:"arial",color:"blue"}}>Comments : <span style={{color:"black"}}>{item.comments}</span></p> */}
                                </div>
                            ))}
                        </div>
                        : <div>
                            Loading...
                    </div>
                }
            </div>
        );
    }
 }
 function mapStateToProps(state){
     return{
         newsItems: state.news,
     };
 }
 function mapDispatchToProps(dispatch){
     return{
         newsActions:bindActionCreators(newsActions , dispatch),
     };
 }
export default connect(mapStateToProps , mapDispatchToProps)(News);
