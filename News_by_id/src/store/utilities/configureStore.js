import {createStore,applyMiddleware,compose} from 'redux';
import rootReducer from '../reducers/newsReducers';
import newsMiddleware from '../middleware/newsService'
import authService from '../middleware/authService';

export default function configureStore() {
return createStore(
    rootReducer,
   compose(applyMiddleware(
       newsMiddleware,
       authService
   ))
);
}