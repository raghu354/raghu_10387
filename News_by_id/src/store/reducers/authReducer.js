import * as allActions from '../actions/actionConstant';


const initialstate ={
    isLoggedIn:false
}

export default function authReducer(state = initialstate,actions){
    switch(actions.type){
        case allActions.LOGIN_USER_SUCCESS:
        return {
            ...state,
            isLoggedIn :true
        }
        default : 
        return state;
    }
}