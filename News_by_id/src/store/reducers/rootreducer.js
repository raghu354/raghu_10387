import { combineReducers } from 'redux';

import counterReducer from './counterReducer';

import newsReducer from './newsReducer';
import authReducer from './authReducer';

const rootReducer = combineReducers({
    counterReducer,
    newsReducer,
    authReducer
});

export default rootReducer;