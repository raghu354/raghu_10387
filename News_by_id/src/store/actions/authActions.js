import * as allActions from '../actions/actionConstant';


export function doLogin(data){
    return{
        type: allActions.DO_LOGIN_USER,
        payload:data
    }
}

export function loginUserSuccess(data){
    return{
        type:allActions.LOGIN_USER_SUCCESS,
        payload:data
    }
}

export function logoutuser(){
    return {
        type:allActions.LOGOUT_USER,
        payload:{}
    }
}