import * as allActions from '../actions/actionConstant';



export function receiveNews(data) {
    console.log("receive actions");
    return {
        type: allActions.RECEIVE_NEWS, payload: data
    }
}

export function fetchNews() {
    console.log("new service");
    return {
        type: allActions.FETCH_NEWS, payload: {}
    }
}


export function receiveNewsbyid(data) {
    console.log("receive actions");
    return {
        type: allActions.RECEIVE_NEWS_By_Id, payload: data
    }
}

export function fetchNewsbyid(id) {
    console.log("new service");
    return {
        type: allActions.FETCH_NEWS_By_Id, payload: {id}
    }
}

