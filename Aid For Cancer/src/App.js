import React, { Component } from 'react';

import './App.css';

import Routes from './component/route';
import News from './component/news'
import Login from './component/login'
// import Newsbyid from './component/newsbyid';

// import Clock from './component/clock';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Routes/>
      </div>
    );
  }
}

export default App;
