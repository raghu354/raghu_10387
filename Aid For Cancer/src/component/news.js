import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsActions';
import {Link} from 'react-router-dom';
import '../AidForCancer.css';


class News extends Component {
    constructor(props) { 
        super(props);
    }
    componentDidMount(){
        this.props.newsActions.fetchNews();
    }
    render(){
        return(
            <div>
                <div className="container ">
                    <div className=" content card"><br />
                        <h3>Information about Cancer</h3><br />
                        <p>Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                    </div>
                </div><br />
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="list-group">
                                <a href="#" class="list-group-item active" style={{ backgroundColor: "#249b8f", color: "white", border: "1px solid #249b8f"}}>News</a>
                                <a href="#" class="list-group-item">NGO's/NPO's</a>
                                <a href="#" class="list-group-item">Support Group</a>
                                <a href="#" class="list-group-item">Online Help</a>
                            </div>
                        </div>
                        <div class="col-lg-9" style={{borderBottom:"2px solid black",height:"25px"}}>
                            <div className ="row">
                                <p className="news" >News</p>
                                <p className="showall" >Show all</p>
                            </div>
                            <div className="row">
                                {
                                    (this.props.newsItems)
                                        ? <div class="container">
                                            {this.props.newsItems.map(item => (
                                                <div class="newsContent" key={item._id}>
                                                    <img class="newsLogo" src={ "http://13.229.176.226:8001/"+item.image} alt=""/>
                                                    <p><Link to={'/news/'+item._id}>{item.heading}</Link></p>
                                                    <p></p>
                                                </div>
                                            ))}
                                        </div>
                                        : <div>
                                            Loading...
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        );
    }
 }
 function mapStateToProps(state){
     return{
         newsItems: state.news,
     };
 }
 
 function mapDispatchToProps(dispatch){
     return{
         newsActions:bindActionCreators(newsActions , dispatch),
     };
 }
export default connect(mapStateToProps , mapDispatchToProps)(News);
