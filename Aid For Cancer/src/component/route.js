import React from 'react';
import {Route} from 'react-router-dom';
import {BrowserRouter as Router} from 'react-router-dom';

import Home from './home';
import News from './news';
import Newsbyid from './newsbyid';
// import Login from './login';
import dashboard from './dashboard';
import Header from './header';
import Footer from './footer';

 


const Routes = () =>(
    <Router>
        <div>
            <Header/>
             <Route path={'/'} exact component={News}/> 
            <Route path={'/news/:id'} component={Newsbyid}/>
            <Footer/>
            {/* <Route path={'/Login'} component ={Login}/> */}
            {/* <Route path={'/admin/dashboard'} component={dashboard}/> */}
        </div>
    </Router>
)

export default Routes;