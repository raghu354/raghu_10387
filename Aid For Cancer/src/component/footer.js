import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer>
                <nav class="navbar navbar-inverse" style={{ backgroundColor: "#ee6e73",height:"70px"}}>
                    <ul class="nav navbar-right">
                        <li class="foot-item" style={{marginLeft:"10vw"}}>
                            <a class="foot-link" href="#" >Contact Us</a>
                        </li>
                        <li class="foot-item">
                            <a class="foot-link" href="#" style={{border:"1px solid white",color:"dodgerblue",padding:"2px"}}>Report Abuse</a>
                        </li>
                        <li class="foot-item" style={{marginLeft:"35vw"}}>
                            <a class="foot-link" href="#">AboutUs</a>
                        </li>
                        <li class="foot-item">
                            <a class="foot-link" href="#">Privacy Policy</a>
                        </li>
                        <li class="foot-item">
                            <a class="foot-link" href="#">Disclaimer</a>
                        </li>
                    </ul>
                </nav>
                <ul style={{backgroundColor:"#e65055",height:"50px"}}>
                    <li style={{padding:"10px",color:"white"}}>© 2019 AidForCancer</li>
                </ul>
            </footer>
                )
            }
        }
export default Footer;