import React, { Component } from 'react';
import '../AidForCancer.css';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <header>
                <nav class="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "#ee6e73" }}>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon">- </span>       
                </button>
                    <a class="navbar-brand" href="#" style={{ color: "white" }}>
                        <u style={{ fontFamily: "Montserrat,sans-serif", marginLeft: "20px" }}>AidforCancer</u>
                    </a>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav  navbar-right mr-auto" style={{marginLeft:"35vw"}}>
                        <li class="nav-item" >
                            <a class="nav-link" href="#" >Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Opinions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Forum</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Volunteer</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Seek Help</a>
                        </li>
                        <li>
                            <a class="nav-link" href="#">
                                <button >sign in</button>
                            </a>
                        </li>
                    </ul>
                    </div>
                </nav>
            </header>
        );
    }
}

export default Header;