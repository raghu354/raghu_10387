// import React, {Component} from 'react';
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import * as newsActions from '../store/actions/newsActions';
// import {Link} from 'react-router-dom';



// class Newsbyid extends Component {
//     constructor(props) {
//         super(props);
//         this.state={
//             newsItems:{}
//         }
    
//     }
//     componentWillMount(){
//         this.props.newsActions.fetchNewsbyid(this.props.match.params.id);
//     }
//     componentWillReceiveProps(newsProps){
//         debugger;
//         if(newsProps.newsItems1){
//             this.setState({
//                 newsItems :newsProps.newsItems1
//             })
//         }
       
//     }
//     render(){
//         return(
//            <div className ="container">
//                <ul class="breadcrumb">
//                <Link to= "/"><li>Information</li></Link>
//                /
//                <li><a href="#">News</a></li></ul> 
//               <h2>{this.state.newsItems.heading}</h2>
//               <p>{this.state.newsItems.heading}</p>
//               <div>
//               {this.state.newsItems.body}
//               </div>
//            </div>
//         );
//     }
//  }
//  function mapStateToProps(state){
 
//      return{
//          newsItems1: state.newsbyid,
//      };
//  }
//  function mapDispatchToProps(dispatch){
//      return{
//          newsActions:bindActionCreators(newsActions , dispatch),
//      };
//  }
// export default connect(mapStateToProps , mapDispatchToProps)(Newsbyid);
import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsActions';
import {Link} from 'react-router-dom';


class Newsbyid extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newsItem : {}
        }
    }
    componentWillMount() {
        this.props.newsActions.fetchNewsById(this.props.match.params.id);
    }
    componentWillReceiveProps(newProps) {
        if(newProps.newsItemFromReducer) {
            this.setState({
                newsItem : newProps.newsItemFromReducer
            })
        }
    }
    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <ul class="breadcrumb">
                        <Link to="/"><li>Information</li></Link>/
                        <li><a href="#">News</a></li>
                    </ul>
                </div>
                <center><img style={{ width: "300px", height: "300px" }} src={"http://13.229.176.226:8001/" + this.state.newsItem.image} alt="" /></center>
                <div style={{ textAlign: "justify" }}>
                    Publish Date : {this.state.newsItem.timestamp}
                </div>
                <div>
                    {this.state.newsItem.body}
                </div>
            </React.Fragment>
        )
    }
}
function mapStateToProps(state) {
    return {
        newsItemFromReducer : state.newsReducer.selectedNews
    }
}
function mapDispatchToProps(dispatch) {
    return {
        newsActions :bindActionCreators(newsActions,dispatch)
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Newsbyid);