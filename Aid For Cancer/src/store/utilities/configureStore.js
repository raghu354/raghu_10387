import {createStore,applyMiddleware,compose} from 'redux';
import rootReducer from '../reducers/newsReducers';
import newsService from '../middleware/newsService'
import authService from '../middleware/authService';

export default function configureStore() {
    debugger
return createStore(
    rootReducer,
   compose(applyMiddleware(
       newsService,
       authService
   ))
);
}