import { combineReducers } from 'redux';

import counterReducer from './counterReducer';

import newsReducer from './newsReducer';
import authReducer from './authReducer';
debugger
const rootReducer = combineReducers({
    counterReducer,
    authReducer,
    newsReducer
    
});

export default rootReducer;