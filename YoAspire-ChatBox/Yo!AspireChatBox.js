 // JQuery Function to display the hiding chat window when we 
            // click on respective anchor tag
            $("#nav a").click(function () {
                $(".toggle").hide();
                var toShow = $(this).attr('href');
                $(toShow).slideToggle(100);   
            });
            // Function for the first contact to add the list tag automatically to the 
            // chat body when user sends a message

            $("#button1").click(function(){
                var date=new Date();
                var datastring=(date.getHours()+":"+date.getMinutes());
                var node = $("#text1").val();
                if(node!=""){
                    $("#myList1").append("<li class='chat-list1'><p class='date' style='float:right'>"+datastring+"</p>"+"<p class='chat-list' style='float:right'>"+node+"</p></li>");
                    $("#p1").html(node);
                    $(".p1").html(datastring);
                }
                $("#text1").val(" ");
                $( "#text1" ).focus(function() {
                    $( this ).next( "#text1" ).css( "display", "inline" ).fadeOut( 1000 );
                  });
            });
            // Function for the second contact to add the list tag automatically to the 
            // chat body when user sends a message

            $("#button2").click(function(){
                var date=new Date();
                var datastring=(date.getHours()+":"+date.getMinutes());
                var node = $("#text2").val();
                if(node!=""){
                    $("#myList2").append("<li class='chat-list1'><p class='date' style='float:right'>"+datastring+"</p>"+"<p class='chat-list' style='float:right'>"+node+"</p></li>");
                    $("#p2").html(node);
                    $(".p2").html(datastring);
                }
                $("#text2").val(" ");
                $( "#text2" ).focus(function() {
                    $( this ).next( "#text2" ).css( "display", "inline" ).fadeOut( 1000 );
                  });
            });
            // Function for the third contact to add the list tag automatically to the 
            // chat body when user sends a message 

            $("#button3").click(function(){
                var date=new Date();
                var datastring=(date.getHours()+":"+date.getMinutes());
                var node = $("#text3").val();
                if(node!=""){
                    $("#myList3").append("<li class='chat-list1'><pre class='date' style='float:right'>"+datastring+"</pre>"+"<pre class='chat-list' style='float:right'>"+node+"</pre></li>");
                    $("#p3").html(node);
                    $(".p3").html(datastring)
                }
                $("#text3").val(" ");
                $( "#text3" ).focus(function() {
                    $( this ).next( "#text3" ).css( "display", "inline" ).fadeOut( 1000 );
                  });
            });

            // Function for the fourth contact to add the list tag automatically to the 
            // chat body when user sends a message

            $("#button4").click(function(){
                var date=new Date();
                var datastring=(date.getHours()+":"+date.getMinutes());
                var node = $("#text4").val();
                if(node!=""){
                    $("#myList4").append("<li class='chat-list1'><pre class='date' style='float:right'>"+datastring+"</pre>"+"<pre class='chat-list' style='float:right'>"+node+"</pre></li>");
                    $("#p4").html(node);
                    $(".p4").html(datastring);
                }
                $("#text4").val(" ");
                $( "#text4" ).focus(function() {
                    $( this ).next( "#text4" ).css( "display", "inline" ).fadeOut( 1000 );
                  });
            });

            // Function for the fifth contact to add the list tag automatically to the 
            // chat body when user sends a message 
            $("#button5").click(function(){
                var date=new Date();
                var datastring=(date.getHours()+":"+date.getMinutes());
                var node = $("#text5").val();
                if(node!=""){
                    $("#myList5").append("<li class='chat-list1'><pre class='date' style='float:right'>"+datastring+"</pre>"+"<pre class='chat-list' style='float:right'>"+node+"</pre></li>");
                    $("#p5").html(node);
                    $(".p5").html(datastring)
                }
                $("#text5").val(" ");
                $( "#text5" ).focus(function() {
                    $( this ).next( "#text5" ).css( "display", "inline" ).fadeOut( 1000 );
                  });
            });
           
        
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#nav *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
      