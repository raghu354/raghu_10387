import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Employee} from './shared/employee';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiURL: string = 'http://dummy.restapiexample.com/api/v1/employees';
  constructor(private httpClient: HttpClient) {}
  getEmployees():Observable <Employee> {
    console.log("fgdjfgdhd")
    return this.httpClient.get<Employee>(this.apiURL)
  }
}
