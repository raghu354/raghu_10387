import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        opacity: 1,
        backgroundColor: 'green'
      })),
      state('closed', style({
        opacity: 0.5,
        backgroundColor: 'yellow gjg'
      })),
      transition('open => closed', [
        animate('1s')
      ]),
    ]),
    
  ]
})
export class AppComponent {
  isOpen = false;
  constructor(private router: Router) { }
  public title = "";
  
}
