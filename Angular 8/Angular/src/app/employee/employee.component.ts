import { Component, OnInit } from '@angular/core';
import { Employee } from "../shared/employee";
import { ApiService } from "../api.service";
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  animations:[
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(5000)),
    ]),
  ]
})



export class EmployeeComponent implements OnInit {
  Employees: Employee;
  
  constructor(private myData: ApiService) { }

  ngOnInit() {
    this.getAllTodos(); 
  }
  
  getAllTodos() {
    this.myData.getEmployees()
      .subscribe(
        (data: Employee) => { //start of (1)
          this.Employees = data;
          if (this.Employees)
            console.log(this.Employees)
          else
            console.log("no hi")
        }, //end of (1)
        (error: any) => console.log(error), //(2) second argument
        () => console.log('all data gets') //(3) second argument
      );
  }  
}




