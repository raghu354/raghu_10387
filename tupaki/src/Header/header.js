 import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style.css';
import {NavLink} from 'react-router-dom';


class Headers extends React.Component {
    render() {
      return (
        <div>
          <div className = "container">
            <div className="logo">
              <a href="/"><img src={"http://static.tupaki.com/publicnew/images/Tupaki_logo1.png"} className="img-responsive"/></a>
            </div>
          </div>
        <nav className="navbar navbar-expand-sm bg-dark ">
        <div className="container">
          <ul className="navbar-nav ">
            <li className="nav-item">
            <NavLink to="/home"  activeStyle={{fontWeight: "bold",backgroundColor:"red"}}><a className="nav-link" href="#">HOME</a></NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/politicalnews"  activeStyle={{fontWeight: "bold",backgroundColor:"red"}}><a className="nav-link" href="#">POLITICAL NEWS</a></NavLink>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">MOVIE NEWS</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">MOVIE REVIEWS</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">PHOTO GALLERY</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">ADVERTISE</a>
            </li>
          </ul>
          <button type="button" className="btn">English</button>
          </div>
        </nav>
        </div>
        )
    }
  }
  export default Headers;