import React, { Component } from 'react';
import './App.css';
import {Route} from 'react-router-dom';
import Headers from './Header/header';
import Viewport from './Body/viewport';
import Footer from './Footer/footer';
import Political from './political/Political';


class App extends Component {
  render() {
    return (
      <div>
          <Headers/>
          <div>
              <Route path ="/" exact component = {Viewport} />
              <Route path = "/politicalnews" component ={Political} />
              <Route path = "/home" component ={Viewport} />
          </div>
          <Footer/>
          
      </div>
    );
  }
}

export default App;
