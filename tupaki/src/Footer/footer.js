import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style.css';

class Footer extends React.Component {
    render() {
      return (
        <div className="footer">
        <div className=" container">    
            <div className="social">
            <a href="https://www.facebook.com/Tupakidotcom" target="_blank"><i className="fa fa-facebook fa-x"></i></a>
            <a href="https://twitter.com/tupaki9999" target="_blank"><i className="fa fa-twitter fa-x"></i></a>
            <a href="#"><i className="fa fa-vimeo-square fa-x"></i></a>
            <a href="#"><i className="fa fa-youtube fa-x"></i></a>
            <a href="#"><i className="fa fa-rss fa-x"></i></a>
            </div> 
            <div className="links">
            <a href="/">Home</a>
            <a href="/politicalnews">Political News</a>
            <a href="/movienews">Movie News</a>
            <a href="/moviereviews">Movie Reviews</a>
            <a href="/photogallery">Photos Gallery</a>
            <a href="/contactus">Contact US</a>
            <a href="/advertise">Advertise</a>
            <a href="/privacypolicy">Privacy Policy</a>
            
            </div>
            
            <div className="apps">
                <a href="/" title="Tupaki App IOS App Store" ><img src={"http://static.tupaki.com/publicnew/images/appstore.png"}/></a>
                <a href="/" target="_blank" ><img src={"http://static.tupaki.com/publicnew/images/playstore.png"}/></a>
            </div>
            <p className="copyright">© Copyright 2018. All rights reserved.</p>
                              
        </div>
        </div>
        )
      }
    }
export default Footer;