import {createStore,applyMiddleware,compose} from 'redux';
import rootReducer from '../reducers/rootreducer';
import newsService from '../middleware/authService'
import authService from '../middleware/newsService';
import userService from '../middleware/userServices';

export default function configureStore() {
return createStore(
    rootReducer,
   compose(applyMiddleware(
       newsService,
       authService,
       userService
   ))
);
}