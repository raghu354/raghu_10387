import { combineReducers } from 'redux';

import counterReducer from './counterReducer';

import newsReducer from './newsReducers';
import authreducer from './authReducer';
import userService from './userReducer';

const rootReducer = combineReducers({
    counterReducer,
    authreducer,
    newsReducer,
    userService
    
});

export default rootReducer;