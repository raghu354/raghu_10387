import * as allActions from '../actions/actionConstant';


const initialstate ={ 
    user : {},
    isUser:false
}


export default function userReducer (state = initialstate,action){
   
    switch(action.type){
        
        case allActions.RECEIVE_USER_DETAILS:
        console.log("userReducer")
                return {
                    ...state,
                    user : action.payload,
                    isUser:true
                }  
        case allActions.USER_DETAILS_FETCH:
        return{
            action,
        }

        case allActions.LOGOUT_USER:
        return{
            ...state,
                    user : null,
                    isUser:false
        }
        default  :
        return state
    }
}