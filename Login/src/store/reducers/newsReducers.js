import * as allActions from '../actions/actionConstant';

const initialise = {
    news :[],
    isLoaded : false,
    newsbyid:{}
}


export default function newReducer(state = initialise,action) {
    switch(action.type) {
        case allActions.FETCH_NEWS:
        return action;

        case allActions.RECEIVE_NEWS:
        return  {
         ...state,
         news : action.payload.articles,
         isLoaded :true
        }

        case allActions.FETCH_NEWS_By_Id:
        return action;

        case allActions.RECEIVE_NEWS_By_Id:
        return  {
         ...state,
         newsbyid : action.payload.article,
         isLoaded :true
        }

        default :
        return state
    }
}