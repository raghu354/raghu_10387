import * as allActions from './actionConstant';

export function doLoginUser(data) {
    return {
        type : allActions.DO_LOGIN_USER ,
        payload : data
    };
}

export function loginUserSuccess(data) {
    return {
        type : allActions.LOGIN_USER_SUCCESS ,
        payload : data
    };
}

