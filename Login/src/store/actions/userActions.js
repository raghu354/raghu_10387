import * as allActions from '../actions/actionConstant';



export function fetchuser() {
    return {
        type :allActions.USER_DETAILS_FETCH,
        payload : {}
    }
}

export function ReceiveUser(data){
    return {
        type : allActions.RECEIVE_USER_DETAILS,
        payload : data
    }
}

