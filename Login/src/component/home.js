import React ,{Component} from 'react';
import {Link} from 'react-router-dom';
import '../AidForCancer.css';
import {Redirect} from 'react-router-dom';
import $ from 'jquery'


class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value:false
        }
    }
    logout= e =>{
        this.setState({
            value :true
        })
        localStorage.removeItem('jwt-token')
        
    }

    render() {
        return (
            <div>
                <div>
                <nav class="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "#ee6e73" }}>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon">- </span>       
                </button>
                    <a class="navbar-brand" href="#" style={{ color: "white" }}>
                        <u style={{ fontFamily: "Montserrat,sans-serif", marginLeft: "20px" }}>AidforCancer</u>
                    </a>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav  navbar-right mr-auto" style={{marginLeft:"35vw"}}>
                        <li class="nav-item" >
                            <a class="nav-link" href="#" >Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Opinions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Forum</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Volunteer</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Seek Help</a>
                        </li >
                        <li className={"nav-item "+ (this.state.value == false)?" show":" hidden" } id="Logout" >
                            <Link to='/'><button onClick={this.logout}>logOut</button></Link>
                        </li>
                        <li className={"nav-item "+ (this.state.value == true)?" show":" hidden" } id="Login" >
                            <Link to='/login'><button>logIn</button></Link>
                        </li>
                    </ul>
                    </div>
                </nav>
            </div>
                     
            </div>
        );
    }
}

export default Header;