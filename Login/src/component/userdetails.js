import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../store/actions/userActions';
import {Redirect} from 'react-router-dom';

class User extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        this.props.userActions.fetchuser();
    }
    render(){
        console.log(this.props.userDetails);
        if(this.props.isUser == true)
        {

            let role=JSON.parse(this.props.userDetails);
            console.log(role.user.role)
            if(role.user.role == "user")
            {
                return  <Redirect to = '/user/dashboard'/>
            }
            else if(role.user.role == "employer"){
                return <Redirect to = '/employer/dashboard'/>
            }
            else if(role.user.role == "recruiter"){
                return <Redirect to = '/recruiter/dashboard'/>
            }
            else if(role.user.role == "interviewer"){
                return <Redirect to = '/interviewer/dashboard'/>
            }

        }
       
        return(
            <div>
                {                  
                    (this.props.userDetails)
                        ? <div class="container">
                            
                        </div>
                        : <div>
                            Loading...
                    </div>
                }
            </div>
        );
    }
 }
 function mapStateToProps(state){
     console.log(state)

     return{
         userDetails: state.userService.user,
         isUser:state.userService.isUser,
     };
 }
 
 function mapDispatchToProps(dispatch){
     return{
         userActions:bindActionCreators(userActions , dispatch),
     };
 }
export default connect(mapStateToProps , mapDispatchToProps)(User);
