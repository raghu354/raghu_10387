import React from 'react';
import {Route} from 'react-router-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import Home from './home';
import Login from './login';
import UserDashboard from './userdashboard';
import AdminDashboard from './admindashboard';
import User from './userdetails';
import Interviewer from './interviewer';
import Recruiter from './recruiter';
import Header from './header';


const Routes = () =>(
    <Router>
        
        <div>
            <Home></Home> 
            <Route path={'/'} exact component ={Login}/>
            <Route path={'/Redirect'} component ={User}/>
            <Route path={'/user/dashboard'} component={UserDashboard}/>
            <Route path={'/employer/dashboard'} component={AdminDashboard}/>
            <Route path={'/interviewer/dashboard'} component={Interviewer}/>
            <Route path={'/recruiter/dashboard'} component={Recruiter}/>
        </div>
    </Router>
)

export default Routes;