
import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux'
import * as authActions from '../store/actions/authActions';
import { Redirect } from 'react-router-dom'
import { Link } from 'react-router-dom'

const div = {
    backgroundImage: "url('http://getwallpapers.com/wallpaper/full/a/5/d/544750.jpg')",
    backgroundSize: "cover",
    width:"400px",
    height:"100%",
    borderradius: "25px",
    marginTop :"150px"
}



class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : '',
            isSubmitted : false
        }
        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();
    }
    doLogin = e => {
        this.props.authActions.doLoginUser({
            email : this.state.email,
            password : this.state.password
        })
        this.setState({
            isSubmitted : true
        })
    }
    render() {
        if (this.state.isSubmitted) {
            if(this.props.isLoggedIn) {
                window.location.reload()
                return <Redirect to = '/redirect'/>
            }
            else  {
                console.log(this.props.statuscode)
              if (this.props.statuscode==-1)
              {
                  alert("Email is wrong")
              }
              else if (this.props.statuscode==-2)
              {
                  alert("password is wrong")
              }
            }
        }        return (
            <div>
                <div>  </div>
                
                <div style={div} className="container">
                    <div class="form-group">
                        <label><b>Email:</b></label>
                        <input type="email" className="form-control" placeholder="Enter email" name="email" value={this.state.email} onChange={this.handleChange} />
                    </div>
                        <div class="form-group">
                            <label><b>Password:</b></label>
                            <input type="password" className="form-control" placeholder="Enter password" name="password" value={this.state.password} onChange={this.handleChange} />
                        </div>
                        <button type="submit" onClick={this.doLogin} className="btn btn-danger">Submit</button>
                </div>
            </div>
        )
    }
}
 
function mapStateToProps(state) {
    debugger
    console.log(state)
    return {
        isLoggedIn : state.authreducer.is_logged_in,
        statuscode : state.authreducer.statuscode
    }
}
function mapDispatchToProps(dispatch) {
    return {
        authActions : bindActionCreators(authActions,dispatch)
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Login);













