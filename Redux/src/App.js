import React, { Component } from 'react';

import './App.css';
import Registered from './component/registered';
import Routes from './component/route';
import News from './component/news'

// import Clock from './component/clock';


class App extends Component {
  render() {
    return (
      <div className="App">
        <News />
        {/* <Clock/> */}
        {/* <Registered/>  */}
        {/* <Routes/> */}
      </div>
    );
  }
}

export default App;
