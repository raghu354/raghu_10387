import {createStore,applyMiddleware,compose} from 'redux';
import rootReducer from '../reducers/newsReducers';
import newsMiddleware from '../middleware/newsService'

export default function configureStore() {
return createStore(
    rootReducer,
   compose(applyMiddleware(
       newsMiddleware
   ))
);
}