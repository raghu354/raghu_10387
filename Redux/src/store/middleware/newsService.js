
import request from 'superagent';
import * as allActions from '../actions/actionConstant';
import * as newsActions from '../actions/newsActions';

const newService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_NEWS:
        console.log("news service");
        request.get("http://13.229.176.226:8001/api/news/recent")
        .then(res => {
            console.log("success");
            const data = JSON.parse(res.text);
            next(newsActions.receiveNews(data));
        })
        .catch(err => {
            console.log("service failure");
            next({ type : 'FETCH_NEWS_DATA_ERROR', err });
        })

        break;

        default:
        break;
    }
}
export default newService;