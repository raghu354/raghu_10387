import { combineReducers } from 'redux';

import counterReducer from './counterReducer';

import newsReducer from './newsReducer';

const rootReducer = combineReducers({
    counterReducer,
    newsReducer
});

export default rootReducer;