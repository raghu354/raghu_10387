import * as allActions from '../actions/actionConstant';

const initialise = {
    news :[],
    isLoaded : false
}

export default function newReducer(state = initialise,action) {
    switch(action.type) {
        case allActions.FETCH_NEWS:
        return action;

        case allActions.RECEIVE_NEWS:
        return  {
         ...state,
         news : action.payload.articles,
         isLoaded :true
        }

        default :
        return state
    }
}