import React from 'react';
import {Route} from 'react-router-dom';
import {BrowserRouter as Router} from 'react-router-dom';
// import Registered from './registered';
// import Clock from './clock';
import Home from './home';
// import Cardetails from './cars';
// import Carpics from './carnode';
// import Fetch from './fetch';
// import Employee from './employee';
// import Fetchpost from './fetchpost';
// import ProductList from './productsdetails';
// import Productadd from './productpost';
// import Product from './product';
// import Productdelete from './Deleteproduct'
import counter from './counter';
import news from './news';
 


const Routes = () =>(
    <Router>
        <div>
             <Route exact path={"/"} component={Home}></Route>
            {/* <Route path={"/clock"} component={Clock}></Route>
            <Route path={"/registerd"} component={Registered}></Route>
            <Route path={"/cars"} exact component={Cardetails}></Route>
            <Route path={"/cars/:id"} exact component={Carpics}></Route>
            <Route path={'/fetch'} exact component={Fetch}></Route>
            <Route path={'/Employee'} component={Employee}></Route>
            <Route path={'/post'} component={Fetchpost}></Route>
            <Route path={'/product'} component={Product}></Route>
            <Route path={'/productdetails'} component={ProductList}></Route>
            <Route path={'/productsadd'}component={Productadd}></Route>
            <Route path={'/productsdelete'} component={Productdelete}/> */} 
            <Route path={'/count'} component={counter}/>
            <Route path={'/news'} component={news}/>
        </div>
    </Router>
)

export default Routes;