import React,{Component} from 'react';


class Registered extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            UserName: '',
            fav: ''
         }
         this.handleChange = this.handleChange.bind(this);
         this.handleSubmit = this.handleSubmit.bind(this);
    } 
    handleChange(event){
        this.setState({[event.target.name] :event.target.value});
    }
    handleSubmit(event){
        alert("A new was submitted:" +this.state.UserName+" "+"YOU FAV fruit:"+this.state.fav)
        event.priventDefault();
    }
    render() { 
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Name:
                    <input type="text" name="UserName" onChange={this.handleChange} />
                </label><br></br>
                <label>
                    Pick your favorite flavor:
                <select Name="fav" onChange={this.handleChange}>
                    <option value="select">select</option>
                    <option value="grapefruit">Grapefruit</option>
                    <option value="lime">Lime</option>
                    <option value="coconut">Coconut</option>
                    <option value="mango">Mango</option>
                </select>
                </label>
                <input type="submit" value="Submit"  />
            </form>
          );
    }
}
 
export default Registered;
