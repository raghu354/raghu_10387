import React, { Component } from 'react';


class DisplayAllEmployees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/displayEmployee')
            .then(response => response.json())
            .then(employeeData => {
                this.setState({
                    isLoaded: true,
                    employees: employeeData
                })
            });
    }
    render() {
        var { isLoaded, employees } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div>

                
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name: </th>
                            <th>Email:</th>
                            <th>Role</th>
                            <th>Favourite Food :</th>
                        </tr>
                    </thead>
                    <tbody>
                    {employees.map(employee => (
                        <tr>
                            <td>{employee.employeeId}</td>
                            <td>{employee.employeeName}</td>
                            <td>{employee.role}</td>
                            <td> {employee.securityQuestion} </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
}
export default DisplayAllEmployees;