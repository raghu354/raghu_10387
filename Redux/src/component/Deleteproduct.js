import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';


class Productdelete extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            productname: " ",
         }     
    }
    handelChange(event){
        this.setState({[event.target.name] :event.target.value});
    }
    handelSubmit(event){
        var data = {
            productname: this.state.productname,
        };
        fetch(' http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/delProducts',{
            method: 'DELETE',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
    }).then((res) => res.json())
    .then(alert("Deleted Succefully"));
    }
    render() { 
        return ( 
            <div className="App" style={{width:"500px",height:"400px",marginLeft:"400px"}}>
                     <h2>Deleting Products</h2>
                        <form onSubmit={this.handelSubmit.bind(this)}>
                            <table className="table table-hover" style={{backgroundColor:"gray"}}>
                                <tr>
                                    <td>Productname:</td><td><input type="text" placeholder="Product Name" name="productname" onChange={this.handelChange.bind(this)} ></input><br></br></td>
                                </tr>
                            </table>  
                                    <input type="submit" value="Delete" style={{backgroundColor:"green",padding:"5px"}} />
                        </form>        
            </div>
         );
    }
}
 
export default Productdelete;