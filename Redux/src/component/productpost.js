import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';


class Productadd extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            product_id: " ",
            productname: " ",
            price: " ",
            quantity: " ",
            productType: " "
         }
         this.handleChange = this.handelChange.bind(this);
    }
    
    handelChange(event){
        this.setState({[event.target.name] :event.target.value});
    }
    handelSubmit(event){
        
        var data = {
            productname: this.state.productname,
            price: this.state.price,
            quantity: this.state.quantity,
            productType: this.state.productType
        };
        fetch(' http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/addProducts',{
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
    }).then((res) => res.json())
    .then(alert("Added Succefully"));
    event.priventDefault();
    }
    render() { 
        return ( 
            <div className="App" style={{width:"500px",height:"400px",marginLeft:"400px"}}>
                     <h2>Adding Products</h2>
        <form onSubmit={this.handelSubmit.bind(this)}>
            <table className="table table-hover" style={{backgroundColor:"gray"}}>
                
                <tr>
                    <td>Productname:</td><td><input type="text" placeholder="Product Name" name="productname" onChange={this.handelChange.bind(this)} ></input><br></br></td>
                </tr>
                <tr>
                    <td>Price:</td><td><input type="text" placeholder="Product price" name="price" onChange={this.handelChange.bind(this)}></input><br></br></td>
                </tr>
                <tr>
                    <td>Quantity:</td><td><input type="text" placeholder="Product quantity" name="quantity" onChange={this.handelChange.bind(this)}></input><br></br></td>
                </tr>
                <tr>
                    <td>ProductType:</td><td><input type="text" placeholder="Product Type" name="productType" onChange={this.handelChange.bind(this)} ></input><br></br></td>
                </tr>
            </table>  
            <input type="submit" value="Submit" style={{backgroundColor:"green",padding:"5px"}} />
        </form>        
            </div>
         );
    }
}
 
export default Productadd;