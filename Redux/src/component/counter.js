import React,{Component} from 'react';
import { connect } from 'react-redux';



class Counter extends Component {
    increment =() => {
       this.props.dispatch({type: "INCREMENT"})
   }

   decrement =() => {
    this.props.dispatch({type: "DECREMENT"})
}
    render() { 
        return (
            <div>
                <h2>Counter</h2>
                <div>
                    <button onClick = {this.increment}>INCREMENT</button><br></br>
                    <span>{this.props.count}</span><br></br>
                    <button onClick = {this.decrement}>DECREMENT</button>
                </div>
            </div>
         );
    }
}
function mapStateToProps(state){
    return{
        count : state.count
    };
} 
export default connect(mapStateToProps)(Counter);