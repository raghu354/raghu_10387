import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';


class Productadd extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            product_id: " ",
            productname: " ",
            price: " ",
            quantity: " ",
            productType: " "
         }
    }
    handelChange(event){
        this.setState ({product_id: event.target.value},
                        {productname:event.target.value},
                        {price:event.target.value},
                        {quantity:event.target.value},
                        {productType:event.target.value}
            )
    }
    

    componentDidMount(){
        var data = {
            product_id: this.state.product_id,
            productname: this.state.productname,
            price: this.state.price,
            quantity: this.state.quantity,
            productType: this.state.productType
        };
        fetch(' http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/addProducts',{
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
    }).then((res) => res.json())
    }
    render() { 
        return ( 
            <div classname="App" style={{width:"500px",height:"400px",backgroundColor:"gray"}}>
            <center><table class="table table-hover">
                <tr>
                    <td>Product_id:</td><td><input type="text" placeholder="Product Id" name="product_id" onhandelChange={this.handelChange.bind(this)} ></input><br></br></td>
                </tr>
                <tr>
                    <td>Productname:</td><td><input type="text" placeholder="Product Name" name="productname" onhandelChange={this.handelChange.bind(this)} ></input><br></br></td>
                </tr>
                <tr>
                    <td>Price:</td><td><input type="text" placeholder="Product price" name="price" onhandelChange={this.handelChange.bind(this)}></input><br></br></td>
                </tr>
                <tr>
                    <td>Quantity:</td><td><input type="text" placeholder="Product quantity" name="quantity" onhandelChange={this.handelChange.bind(this)}></input><br></br></td>
                </tr>
                <tr>
                    <td>ProductType:</td><td><input type="text" placeholder="Product Type" name="productType" onhandelChange={this.handelChange.bind(this)} ></input><br></br></td>
                </tr>
            </table></center>
            <br></br>
                        {/* <button type="button" class="btn btn-success">Add Product</button> */}
            </div>
         );
    }
}
 
export default Productadd;