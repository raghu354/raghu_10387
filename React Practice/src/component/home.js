import React ,{Component} from 'react';
import {Link} from 'react-router-dom';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
                <ol>
                    <Link to="/clock"><li>Clock</li></Link> 
                    <Link to="/registerd"><li>Registered</li></Link> 
                    <Link to="/cars"><li>Cardetails</li></Link> 
                    <Link to="/fetch"><li>fetch</li></Link>
                    <Link to="/post"><li>post</li></Link>
                    <Link to="/Employee"><li>Employee</li></Link>
                    <Link to="/products"><li>products</li></Link>
                    <Link to="/productsadd"><li>productsadd</li></Link>
                </ol>
            </div>
         );
    }
}
 
export default Home;