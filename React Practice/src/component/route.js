import React from 'react';
import {Route} from 'react-router-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import Registered from './registered';
import Clock from './clock';
import Home from './home';
import Cardetails from './cars';
import Carpics from './carnode';
import Fetch from './fetch';
import Employee from './employee';
import Fetchpost from './fetchpost';
import ProductList from './products';
import Productadd from './productpost';
 


const Routes = () =>(
    <Router>
        <div>
            <Route exact path={"/"} component={Home}></Route>
            <Route path={"/clock"} component={Clock}></Route>
            <Route path={"/registerd"} component={Registered}></Route>
            <Route path={"/cars"} exact component={Cardetails}></Route>
            <Route path={"/cars/:id"} exact component={Carpics}></Route>
            <Route path={'/fetch'} exact component={Fetch}></Route>
            <Route path={'/Employee'} component={Employee}></Route>
            <Route path={'/post'} component={Fetchpost}></Route>
            <Route path={'/products'} component={ProductList}></Route>
            <Route path={'/productsadd'}component={Productadd}></Route>
        </div>
    </Router>
)

export default Routes;